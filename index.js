'use strict';

/**
 * Pagean test framework.
 *
 * @module pagean
 */
const puppeteer = require('puppeteer');

const testLogger = require('./lib/logger');
const testReporter = require('./lib/reporter');
const { ...testFunctions } = require('./lib/tests');
const { createLinkChecker } = require('./lib/link-utils');

/**
 * Executes Pagean tests as specified in config and reports results.
 *
 * @param {object} config The Pagean test configuration.
 * @static
 */
// eslint-disable-next-line max-lines-per-function
const executeAllTests = async (config) => {
    const logger = testLogger(config);
    const linkChecker = createLinkChecker();
    const browser = await puppeteer.launch(config.puppeteerLaunchOptions);

    for (const testUrl of config.urls) {
        logger.startUrlTests(testUrl.rawUrl);

        const consoleLog = [];
        const page = await browser.newPage();
        page.on('console', (message) =>
            consoleLog.push({
                type: message.type(),
                text: message.text(),
                location: message.location()
            })
        );
        await page.goto(testUrl.url, { waitUntil: 'load' });

        const testContext = {
            page,
            consoleLog,
            urlSettings: {
                ...testUrl.settings,
                htmlHintConfig: config.htmlHintConfig
            },
            logger,
            linkChecker
        };
        for (const test of Object.keys(testFunctions)) {
            await testFunctions[test](testContext);
        }

        await page.close();
    }
    await browser.close();
    const testResults = logger.getTestResults();
    testReporter.saveReports(testResults, config.reporters);

    if (testResults.summary.failed > 0) {
        // For test harness want process to exit with error code
        // eslint-disable-next-line unicorn/no-process-exit, no-process-exit, no-magic-numbers
        process.exit(2);
    }
};

module.exports.executeAllTests = executeAllTests;
