'use strict';

/**
 * Creates an instance of a logger object to manage logging functions.
 *
 * @module logger
 */

const consoleLogger = require('ci-logger');
const { reporterTypes } = require('../lib/reporter');

const testResultSymbols = Object.freeze({
    passed: ' √',
    failed: ' ×',
    warning: ' ‼'
});

const nullFunction = () => {};

/**
 * Factory function that creates an instance of a
 * logger object to manage logging functions.
 *
 * @param   {object} config Pagean configuration.
 * @returns {object}        A logger object.
 * @static
 */
// eslint-disable-next-line max-lines-per-function
module.exports = (config) => {
    const cliReporter = {
        // If cli reporter is enabled, set console log function, otherwise null function
        log: config.reporters.includes(reporterTypes.cli)
            ? consoleLogger.log
            : nullFunction,
        // The logAlways function provides a mechanism to log to the console even
        // when the cli reporter is disabled (e.g. returning the final results)
        logAlways: consoleLogger.log
    };

    const testResults = {
        project: config.project,
        executionStart: new Date(),
        summary: {
            tests: 0,
            passed: 0,
            warning: 0,
            failed: 0
        },
        results: []
    };

    let currentUrlTests;
    /**
     * Indicates the start of testing the specified URL. Subsequent
     * calls to logTestResults will be logged with this URL.
     *
     * @instance
     * @param {string} url The URL being tested.
     */
    const startUrlTests = (url) => {
        const urlTestResults = {
            url,
            tests: []
        };
        testResults.results.push(urlTestResults);
        currentUrlTests = urlTestResults;
        cliReporter.log({ message: `\nTesting URL: ${url}` });
    };

    /**
     * Logs test results for the current URL (indicated
     * by the last call to StartUrlTests).
     *
     * @instance
     * @param {object} testResult The test results.
     */
    const logTestResults = (testResult) => {
        if (testResult) {
            currentUrlTests.tests.push(testResult);
            testResults.summary.tests++;
            testResults.summary[testResult.result]++;

            cliReporter.log({
                message: `${testResult.name} (${testResult.result})`,
                isResult: true,
                resultPrefix: testResultSymbols[testResult.result]
            });
        }
    };

    const outputTestSummary = () => {
        cliReporter.logAlways({
            message: `\n Tests: ${testResults.summary.tests}\n Passed: ${testResults.summary.passed}\n Warning: ${testResults.summary.warning}\n Failed: ${testResults.summary.failed}\n`
        });
    };

    /**
     * Outputs the test results summary to stdout and returns
     * the consolidated results for all tests.
     *
     * @instance
     * @returns {object} The consolidated results for all tests.
     */
    const getTestResults = () => {
        outputTestSummary();
        return testResults;
    };

    return {
        startUrlTests,
        logTestResults,
        getTestResults
    };
};
