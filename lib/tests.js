'use strict';

/**
 * Pagean tests.
 *
 * @module tests
 */
const { HTMLHint } = require('htmlhint');

const { testResultStates, pageanTest } = require('./test-utils');
const fileUtils = require('./external-file-utils');
const { isFailedResponse } = require('./link-utils');

const msPerSec = 1000;

/**
 * Tests the current page for the existence of a horizontal scroll bar.
 *
 * @param {object} context Test execution context.
 * @static
 */
const horizontalScrollbarTest = async (context) => {
    await pageanTest(
        'should not have a horizontal scrollbar',
        // eslint-disable-next-line no-shadow -- less intuitive
        async (context) => {
            // istanbul ignore next: injects script causing puppeteer error, see #48
            const scrollbar = await context.page.evaluate(() => {
                document.scrollingElement.scrollLeft = 1;
                return document.scrollingElement.scrollLeft === 1;
            });
            return {
                result:
                    scrollbar === false
                        ? testResultStates.passed
                        : testResultStates.failed
            };
        },
        context,
        'horizontalScrollbarTest'
    );
};

/**
 * Tests the current page for any console output.
 *
 * @param {object} context Test execution context.
 * @static
 */
const consoleOutputTest = (context) => {
    pageanTest(
        'should not have console output',
        // eslint-disable-next-line no-shadow -- less intuitive
        (context) => {
            const testResult = {
                result:
                    context.consoleLog.length === 0
                        ? testResultStates.passed
                        : testResultStates.failed
            };
            if (testResult.result === testResultStates.failed) {
                testResult.data = context.consoleLog;
            }
            return testResult;
        },
        context,
        'consoleOutputTest'
    );
};

/**
 * Tests the current page for any console errors.
 *
 * @param {object} context Test execution context.
 * @static
 */
const consoleErrorTest = (context) => {
    pageanTest(
        'should not have console errors',
        // eslint-disable-next-line no-shadow -- less intuitive
        (context) => {
            const browserErrorLog = context.consoleLog.filter(
                (log) => log.type === 'error'
            );
            const testResult = {
                result:
                    browserErrorLog.length === 0
                        ? testResultStates.passed
                        : testResultStates.failed
            };
            if (testResult.result === testResultStates.failed) {
                testResult.data = browserErrorLog;
            }
            return testResult;
        },
        context,
        'consoleErrorTest'
    );
};

/**
 * Tests the current page for any HTML lint issues.
 *
 * @param {object} context Test execution context.
 * @static
 */
const renderedHtmlTest = async (context) => {
    await pageanTest(
        'should have valid rendered HTML',
        // eslint-disable-next-line no-shadow -- less intuitive
        async (context) => {
            const html = await context.page.content();
            const lintResults = HTMLHint.verify(
                html,
                context.urlSettings.htmlHintConfig
            );
            const testResult = {
                result:
                    lintResults.length === 0
                        ? testResultStates.passed
                        : testResultStates.failed
            };
            if (testResult.result === testResultStates.failed) {
                testResult.data = lintResults;
            }
            return testResult;
        },
        context,
        'renderedHtmlTest'
    );
};

/**
 * Tests the current page for load time.
 *
 * @param {object} context Test execution context.
 * @static
 */
// eslint-disable-next-line max-lines-per-function
const pageLoadTimeTest = async (context) => {
    const testSettingName = 'pageLoadTimeTest';
    await pageanTest(
        'should load page within timeout',
        // eslint-disable-next-line no-shadow -- less intuitive
        async (context) => {
            const { pageLoadTimeThreshold } = context.testSettings;
            const name = `should load page within ${pageLoadTimeThreshold} sec`;
            // istanbul ignore next: injects script causing puppeteer error, see #48
            const performanceTiming = JSON.parse(
                await context.page.evaluate(() =>
                    JSON.stringify(window.performance)
                )
            );
            const loadTimeSec =
                (performanceTiming.timing.loadEventEnd -
                    performanceTiming.timing.navigationStart) /
                msPerSec;
            const testResult = {
                name,
                result:
                    loadTimeSec < pageLoadTimeThreshold
                        ? testResultStates.passed
                        : testResultStates.failed
            };
            if (testResult.result === testResultStates.failed) {
                testResult.data = { pageLoadTime: loadTimeSec };
            }
            return testResult;
        },
        context,
        testSettingName
    );
};

/**
 * Tests the current page for any external JavaScript files and
 * downloads the files for further analysis.
 *
 * @param {object} context Test execution context.
 * @static
 */
// eslint-disable-next-line max-lines-per-function
const externalScriptTest = async (context) => {
    await pageanTest(
        'should not have external scripts',
        // eslint-disable-next-line no-shadow -- less intuitive
        async (context) => {
            // istanbul ignore next: injects script causing puppeteer error, see #48
            const scripts = await context.page.evaluate(() => {
                return [...document.querySelectorAll('script[src]')].map(
                    (s) => s.src
                );
            });

            const pageUrl = context.page.url();
            const externalScripts = scripts.filter((script) =>
                fileUtils.shouldSaveFile(script, pageUrl)
            );
            const scriptResults = await Promise.all(
                externalScripts.map((script) =>
                    fileUtils.saveExternalScript(script)
                )
            );
            const testResult = {
                result:
                    scriptResults.length > 0
                        ? testResultStates.failed
                        : testResultStates.passed
            };
            if (testResult.result === testResultStates.failed) {
                testResult.data = scriptResults;
            }
            return testResult;
        },
        context,
        'externalScriptTest'
    );
};

/**
 * Tests the current page for any broken links (external or within the page).
 *
 * @param {object} context Test execution context.
 * @static
 */
// eslint-disable-next-line max-lines-per-function
const brokenLinkTest = async (context) => {
    await pageanTest(
        'should not have broken links',
        // eslint-disable-next-line no-shadow -- less intuitive
        async (context) => {
            // istanbul ignore next: injects script causing puppeteer error, see #48
            const links = await context.page.evaluate(() => {
                return [...document.querySelectorAll('a[href]')].map(
                    (a) => a.href
                );
            });

            // All links are returned from puppeteer as absolute links, so this filters out
            // javascript and other values and leaves only pages to request.
            const httpLinks = links.filter((link) =>
                link.match(/(http(s?)|file):\/\//)
            );
            // Reduce to unique page links so only checked once
            const uniqueHttpLinks = [...new Set(httpLinks)];

            // Check each link includes check against ignored list, and if not checks
            // both links within the page as well as to other pages
            const linkResponses = await Promise.all(
                uniqueHttpLinks.map(async (link) => ({
                    href: link,
                    status: await context.linkChecker.checkLink(context, link)
                }))
            );

            // Returned results includes status for all links, so filter down to only failed
            const failedLinkResponses = linkResponses.filter((result) =>
                isFailedResponse(result)
            );
            const testResult = {
                result:
                    failedLinkResponses.length > 0
                        ? testResultStates.failed
                        : testResultStates.passed
            };
            if (testResult.result === testResultStates.failed) {
                testResult.data = failedLinkResponses;
            }
            return testResult;
        },
        context,
        'brokenLinkTest'
    );
};

module.exports.consoleErrorTest = consoleErrorTest;
module.exports.consoleOutputTest = consoleOutputTest;
module.exports.horizontalScrollbarTest = horizontalScrollbarTest;
module.exports.pageLoadTimeTest = pageLoadTimeTest;
module.exports.renderedHtmlTest = renderedHtmlTest;
module.exports.externalScriptTest = externalScriptTest;
module.exports.brokenLinkTest = brokenLinkTest;
