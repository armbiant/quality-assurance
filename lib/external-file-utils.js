'use strict';

/**
 * Utilities for checking external JavaScript files.
 *
 * @module external-file-utils
 */
const fs = require('fs');
const path = require('path');

const axios = require('axios');

const externalFilePath = 'pagean-external-scripts';

/**
 * Checks if the JavaScript is external to the page and should be saved.
 *
 * @param   {string}  script The URL of the JavaScript file.
 * @param   {string}  page   The URL of the current page.
 * @returns {boolean}        True if the script is external to the page.
 * @static
 */
const shouldSaveFile = (script, page) => {
    const scriptUrl = new URL(script);
    const pageUrl = new URL(page);
    return scriptUrl.host !== pageUrl.host;
};

/**
 * Loads the JavaScript file from the specified URL and
 * saves it to disc.
 *
 * @param   {string} script The URL of the JavaScript file.
 * @returns {object}        An object with original script
 *                          URL and local file name.
 * @static
 */
const saveExternalScript = async (script) => {
    const result = { url: script };
    try {
        const scriptUrl = new URL(script);
        const pathName = path.join(
            externalFilePath,
            scriptUrl.hostname,
            scriptUrl.pathname
        );
        if (!fs.existsSync(pathName)) {
            // Axios will throw for any error response
            const response = await axios.get(script);
            fs.mkdirSync(path.dirname(pathName), { recursive: true });
            fs.writeFileSync(pathName, response.data);
        }
        result.localFile = pathName;
    } catch (error) {
        result.error = error.message;
    }
    return result;
};

module.exports.saveExternalScript = saveExternalScript;
module.exports.shouldSaveFile = shouldSaveFile;
