'use strict';

/**
 * Utilities for checking page links.
 *
 * @module link-utils
 */

const https = require('https');
const axios = require('axios');
const normalizeUrl = require('normalize-url');

const msPerSec = 1000;
const timeoutSeconds = 120;

/**
 * Enum for HTTP responses.
 *
 * @readonly
 * @enum {number}
 * @public
 */
const httpResponse = Object.freeze({
    continue: 100,
    ok: 200,
    badRequest: 400,
    notFound: 404,
    tooManyRequests: 429,
    unknownError: 999
});

const noRetryResponses = new Set([httpResponse.tooManyRequests]);

/**
 * Normalizes a URL (with https://www.npmjs.com/package/normalize-url).
 * Uses defaults plus the following overrides.
 * 1. Set default protocol to https if protocol-relative.
 * 2. Do not remove any querystring parameters.
 * 3. Strip hash from URL.
 * 4. Do not strip "www." from the URL.
 *
 * @param   {string} url The URL to normalize.
 * @returns {string}     The normalized URL.
 * @static
 * @public
 */
const normalizeLink = (url) =>
    normalizeUrl(url, {
        defaultProtocol: 'https:',
        removeQueryParameters: [],
        stripHash: true,
        stripWWW: false
    });
/* eslint-enable jsdoc/require-description-complete-sentence */

/**
 * Checks settings to determine if the provided link should be ignored.
 *
 * @param   {object}  settings Test settings object, which may contain an
 *                             ignoredLinks array.
 * @param   {string}  link     The link to check against the ignore list.
 * @returns {boolean}          True if the link should be ignored, otherwise false.
 * @static
 * @private
 */
const ignoreLink = (settings, link) =>
    settings.ignoredLinks && settings.ignoredLinks.includes(link);

/**
 * Checks a response to an HTTP request, either a response code or explicit error,
 * to identify any failed responses.
 *
 * @param   {(string|number)} response The response to an HTTP request to check for failure.
 * @returns {boolean}                  True if failed, otherwise false.
 * @static
 * @public
 */
const isFailedResponse = (response) =>
    Number.isNaN(Number(response.status)) ||
    response.status >= httpResponse.badRequest;

/**
 * Checks a Puppeteer page for the element specified in the hash of the provided link.
 *
 * @param   {object}          page A Puppeteer page object.
 * @param   {string}          link The link to check.
 * @returns {(string|number)}      The link status (HTTP response code or error).
 * @static
 * @private
 */
const checkSamePageLink = async (page, link) => {
    const selector = link.slice(page.url().length);
    if (selector === '#' || selector === '#top') {
        return httpResponse.ok;
    }

    const element = await page.$(selector);
    return element ? httpResponse.ok : `${selector} Not Found`;
};

/**
 * Checks the provided link for validity by loading in a Puppeteer page.
 *
 * @param   {object}          page A Puppeteer page object.
 * @param   {string}          link The link to check.
 * @returns {(string|number)}      The link status (HTTP response code or error).
 * @static
 * @private
 */
const checkExternalPageLinkBrowser = async (page, link) => {
    let status;
    try {
        const testPage = await page.browser().newPage();
        const response = await testPage.goto(link);
        status = response.status();
        await testPage.close();
    } catch (error) {
        // Errors are returned in the format: "ENOTFOUND at https://this.url.does.not.exist/",
        // so extract error only and remove URL
        status = error.message.replace(/^(.*) at .*$/, '$1');
    }
    return status;
};

/**
 * Checks the provided link for validity by requesting with axios. If useGet if false,
 * a HEAD request is made for efficiency. If useGet is true, a full GET request is made.
 *
 * @param   {object}          page     A Puppeteer page object.
 * @param   {string}          link     The link to check.
 * @param   {boolean}         [useGet] Used to identify the request method to use (HEAD or GET).
 * @returns {(string|number)}          The link status (HTTP response code or error).
 * @static
 * @private
 */
// eslint-disable-next-line sonarjs/cognitive-complexity -- Allow less than 10
const checkExternalPageLink = async (page, link, useGet = false) => {
    // Get user-agent from page so axios uses the same value for requesting links
    const userAgent = await page.evaluate('navigator.userAgent');

    try {
        const options = {
            headers: { 'User-Agent': userAgent },
            timeout: timeoutSeconds * msPerSec
        };
        // Using internal browser property since not exposed
        // eslint-disable-next-line no-underscore-dangle
        if (page.browser()._ignoreHTTPSErrors) {
            const agent = new https.Agent({ rejectUnauthorized: false });
            options.httpsAgent = agent;
        }
        const httpMethod = useGet ? axios.get : axios.head;
        const response = await httpMethod(link, options);
        return response.status;
    } catch (error) {
        // Some servers respond invalid for head request (e.g. a lot of 405 Method
        // Not Allowed), so if a response was returned, is not a response that should
        // skip a retry (e.g. 429), and the request was head then retry with get.
        if (
            error.response &&
            !noRetryResponses.has(error.response.status) &&
            !useGet
        ) {
            return checkExternalPageLink(page, link, true);
        }

        // Axios will throw for failed response (code >= 400), so return
        // that response code or the error code if execution error
        return error.response ? error.response.status : error.code;
    }
};

/**
 * Factory function returning a linkChecker object with a {@link checkLink}
 * function that caches checked link results.
 *
 * @returns {object} Link checker object.
 * @static
 * @public
 */
const createLinkChecker = () => {
    const checkedLinks = new Map();

    return {
        /**
         * Checks the provided link for validity using context object for a
         * reference to the Puppeteer page and applicable settings.
         *
         * @instance
         * @param   {object}          context A Pagean test context object.
         * @param   {string}          link    The link to check.
         * @returns {(string|number)}         The link status (HTTP response code or error).
         * @public
         */
        // eslint-disable-next-line sonarjs/cognitive-complexity
        checkLink: async (context, link) => {
            let status = httpResponse.unknownError;
            try {
                // Check all page links first since normalize removes the hash
                if (link.startsWith(`${context.page.url()}#`)) {
                    return await checkSamePageLink(context.page, link);
                }

                const normalizedLink = normalizeLink(link);
                if (ignoreLink(context.testSettings, normalizedLink)) {
                    // Set to a unique status so obvious that it's ignored
                    return httpResponse.continue;
                }

                if (
                    context.testSettings.ignoreDuplicates &&
                    checkedLinks.has(link)
                ) {
                    return checkedLinks.get(link);
                }

                status = await (context.testSettings.checkWithBrowser
                    ? checkExternalPageLinkBrowser(context.page, normalizedLink)
                    : checkExternalPageLink(context.page, normalizedLink));
            } catch {
                status = httpResponse.unknownError;
            }

            checkedLinks.set(link, status);
            return status;
        }
    };
};

module.exports.createLinkChecker = createLinkChecker;
module.exports.httpResponse = httpResponse;
module.exports.isFailedResponse = isFailedResponse;
module.exports.normalizeLink = normalizeLink;
