'use strict';

/**
 * Functions for managing Pagean config files.
 *
 * @module config
 */

const fs = require('fs');
const path = require('path');
const Ajv = require('ajv').default;
const ajvErrors = require('ajv-errors');
const protocolify = require('protocolify');

const { normalizeLink } = require('./link-utils');

const defaultConfig = require('./default-config.json');
const defaultHtmlHintConfigFilename = './.htmlhintrc';

const getUrl = (testUrl) => {
    let rawUrl, urlSettings;
    if (typeof testUrl === 'object') {
        rawUrl = testUrl.url;
        urlSettings = testUrl.settings;
    } else {
        rawUrl = testUrl;
    }
    return { rawUrl, urlSettings };
};

const processTestSetting = (setting) => {
    if (typeof setting === 'object') {
        return setting;
    }
    return { enabled: setting };
};

const processAllTestSettings = (settings) => {
    if (!settings) {
        return;
    }
    const processedSettings = {};
    for (const key of Object.keys(settings)) {
        processedSettings[key] = processTestSetting(settings[key]);
    }
    return processedSettings;
};

const consolidateTestSettings = (
    defaultSettings,
    globalSettings,
    urlSettings
) => {
    const sanitizedGlobalSettings = globalSettings || {};
    const sanitizedUrlSettings = urlSettings || {};
    const processedSettings = {};
    for (const key of Object.keys(defaultSettings)) {
        processedSettings[key] = {
            ...defaultSettings[key],
            ...sanitizedGlobalSettings[key],
            ...sanitizedUrlSettings[key]
        };
    }
    if (processedSettings.brokenLinkTest.ignoredLinks) {
        processedSettings.brokenLinkTest.ignoredLinks =
            processedSettings.brokenLinkTest.ignoredLinks.map((link) =>
                normalizeLink(link)
            );
    }
    return processedSettings;
};

const processUrls = (config) => {
    const processedConfigSettings = processAllTestSettings(config.settings);
    return config.urls.map((testUrl) => {
        const { rawUrl, urlSettings } = getUrl(testUrl);
        return {
            url: protocolify(rawUrl),
            rawUrl,
            settings: consolidateTestSettings(
                defaultConfig.settings,
                processedConfigSettings,
                processAllTestSettings(urlSettings)
            )
        };
    });
};

const getHtmlHintConfig = (htmlHintConfigFilename) => {
    if (!fs.existsSync(htmlHintConfigFilename)) {
        return;
    }
    return JSON.parse(fs.readFileSync(htmlHintConfigFilename, 'utf8'));
};

const validateConfigSchema = (config) => {
    const schema = JSON.parse(
        fs.readFileSync(
            path.join(__dirname, '../', 'schemas', 'pageanrc.schema.json')
        )
    );
    const ajv = new Ajv({ allErrors: true });
    ajvErrors(ajv);
    const validate = ajv.compile(schema);
    const isValid = validate(config);
    return { isValid, errors: validate.errors || [] };
};

const getConfigFromFile = (configFileName) => {
    return JSON.parse(fs.readFileSync(configFileName, 'utf8'));
};

/**
 * Loads config from file and returns consolidated config with
 * defaults where values are not specified.
 *
 * @param   {string}    configFileName Pagean configuration file name.
 * @returns {object}                   Consolidated Pagean configuration.
 * @throws  {TypeError}                Throws if config file has an invalid schema.
 * @static
 */
const processConfig = (configFileName) => {
    const config = getConfigFromFile(configFileName);
    const { isValid } = validateConfigSchema(config);
    if (!isValid) {
        throw new TypeError(
            `File ${configFileName} has an invalid pageanrc schema`
        );
    }
    return {
        project: config.project || '',
        puppeteerLaunchOptions: config.puppeteerLaunchOptions,
        urls: processUrls(config),
        htmlHintConfig: getHtmlHintConfig(
            config.htmlhintrc || defaultHtmlHintConfigFilename
        ),
        reporters: config.reporters || defaultConfig.reporters
    };
};

/**
 * Lints the configuration file schema.
 *
 * @param   {string}  configFileName Pagean configuration file name.
 * @returns {boolean}                True if file has valid Pagean config schema.
 * @static
 */
const lintConfigFile = (configFileName) => {
    const config = getConfigFromFile(configFileName);
    return validateConfigSchema(config);
};

module.exports = processConfig;
module.exports.lintConfigFile = lintConfigFile;
