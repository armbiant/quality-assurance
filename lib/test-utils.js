'use strict';

/**
 * Miscellaneous test utilities.
 *
 * @module test-utils
 */

const testResultStates = Object.freeze({
    passed: 'passed',
    warning: 'warning',
    failed: 'failed'
});

const getTestSettings = (testSettingProperty, urlSettings) => {
    if (urlSettings[testSettingProperty] === undefined) {
        throw new Error(
            `Property '${testSettingProperty}' could not be found in test settings`
        );
    }
    return urlSettings[testSettingProperty];
};

/**
 * Wrapper function for executing all Pagean tests that manages getting
 * test-specific settings from configuration, passing the test context,
 * and logging results.
 *
 * @param {string}   name                The name of the test.
 * @param {Function} testFunction        The test function to be executed.
 * @param {object}   testContext         The test execution context.
 * @param {string}   testSettingProperty The name of the config property
 *                                       with settings for the current test.
 * @static
 */
const pageanTest = async (
    name,
    testFunction,
    testContext,
    testSettingProperty
    // eslint-disable-next-line sonarjs/cognitive-complexity -- Allow < 10
) => {
    const testSettings = getTestSettings(
        testSettingProperty,
        testContext.urlSettings
    );
    testContext.testSettings = testSettings;
    if (testSettings.enabled) {
        let results;
        try {
            results = await testFunction(testContext);
        } catch (error) {
            results = { result: testResultStates.failed, data: { error } };
        }
        if (
            results.result === testResultStates.failed &&
            testSettings.failWarn
        ) {
            results.result = testResultStates.warning;
        }
        testContext.logger.logTestResults({
            name,
            ...results
        });
    }
};

module.exports.testResultStates = testResultStates;
module.exports.pageanTest = pageanTest;
