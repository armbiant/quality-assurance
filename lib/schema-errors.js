'use strict';

/**
 * Generates formatted Pagean config schema errors for output to stdout.
 *
 * @module schema-errors
 */
const { red } = require('kleur');

const getDataKey = (instancePath) => {
    const baseKey = '<pageanrc>';
    // Special handling for empty pointer or root element
    if (!instancePath || instancePath === '/') {
        return baseKey;
    }
    // Convert pointer to key be splitting by /, removing first value (since pointer
    // starts with /, so empty), unencoding values, and the re-assembling with different
    // formatting for numeric array indices versus properties.
    return (
        baseKey +
        instancePath
            .split('/')
            .slice(1)
            // eslint-disable-next-line unicorn/no-array-reduce -- reduce for string concatenation
            .reduce((accumulator, currentValue) => {
                const unencodedValue = currentValue
                    .replace(/~0/g, '~')
                    .replace(/~1/g, '/');
                const encodedValue = Number.isNaN(Number(unencodedValue))
                    ? `.${unencodedValue}`
                    : `[${unencodedValue}]`;
                return `${accumulator}${encodedValue}`;
            }, '')
    );
};

const processErrorParameters = (error) => {
    let formattedMessage = error.message;
    // Ajv includes all allowed values in params.allowedValues,
    // so add to message if available.
    if (error.params.allowedValues) {
        formattedMessage += ` (${error.params.allowedValues.join(', ')})`;
    }
    return formattedMessage;
};

/**
 * Generates formatted schema strings for output to stdout for all
 * schema errors.
 *
 * @param   {object[]} errors Array of Pagean config schema errors.
 * @returns {string[]}        Array of formatted schema error strings.
 * @static
 */
const formatErrors = (errors) => {
    const margin = 2;
    let maxLength = 0;
    for (const error of errors) {
        error.dataKey = getDataKey(error.instancePath);
        error.formattedMessage = processErrorParameters(error);
        // Get max dataKey length for all errors to line up column in final output
        if (error.dataKey.length > maxLength) {
            maxLength = error.dataKey.length;
        }
    }
    return errors.map(
        (error) =>
            `  ${error.dataKey.padEnd(maxLength + margin)}${red(
                error.formattedMessage
            )}`
    );
};

module.exports.formatErrors = formatErrors;
