# Version Upgrade Guide

## Upgrading from v4.x to v5.0

1. Update to a Node.js current or LTS release (`^12.20.0 || ^14.15.0 || >=16.0.0`). Pagean may still work in other releases, but is not specifically tested or supported.

2. The Broken Link Test default setting was originally to fail with warning, but this was updated to fail (without warning) to be consistent with the other tests. To return to the previous behavior, update your configuration settings with:

```json
{
  "settings": {
    "brokenLinkTest": {
      "failWarn": true
    }
  }
}
```

## Upgrading from v3.x (`page-load-tests`) to v4.0 (`pagean`)

1. Change the configurations file name from `.pltconfig.json` to `.pageanrc.json`

2. Pagean v4.0.0 updated the configuration file format to allow test-specific settings. With this change, `pageLoadTimeThreshold` must now appears as a setting of the `pageLoadTimeTest` test, rather than at the same level as the other tests. See the example from/to below:

**From:**

```json
{
  "settings": {
    "pageLoadTimeThreshold": 2
  },
  "urls": [
    "https://somewhere.com/",
    {
      "url": "https://somewhere-else.com",
      "settings": {
        "pageLoadTimeThreshold": 3
      }
    }
  ]
}
```

**To:**

```json
{
  "settings": {
    "pageLoadTimeTest": {
      "pageLoadTimeThreshold": 2
    }
  },
  "urls": [
    "https://somewhere.com/",
    {
      "url": "https://somewhere-else.com",
      "settings": {
        "pageLoadTimeTest": {
          "pageLoadTimeThreshold": 3
        }
      }
    }
  ]
}
```
