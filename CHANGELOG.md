# Changelog

## Unreleased

### Fixed

- Updated page load time tests to remove external resources. (#155)
- Update `npm-check` ignore syntax for issue in v6.0.0. (#151)

## v7.0.0 (2022-06-15)

### Changed

- BREAKING: Updated console log properties names to be more intuitive (`text`, `type`, and `location`).
- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#147, #148)

### Fixed

- Updated to latest dependencies, including `puppeteer` v14.4.0

### Miscellaneous

- Changed all CI pipeline references from `master` branch to `CI_DEFAULT_BRANCH`. (#146)
- Added test coverage threshold requirements (#149)

## v6.0.9 (2022-04-27)

### Fixed

- Updated to latest dependencies, including `htmlhint` v1.1.4 and `puppeteer` v13.6.0

### Miscellaneous

- Added `prettier` and disabled `eslint` formatting rules. (#144)
- Moved project back to GitLab flow branching model. (#141)

## v6.0.8 (2022-03-28)

### Fixed

- Update to `htmlhint` v1.1.3, resolving CVE-2020-28469
- Updated tests for `ajv` message changes in v8.11.0
- Refactored to comply with latest `eslint` rules
- Updated to latest dependencies, including resolving CVE-2021-44906 (dev only)

### Miscellaneous

- Updated project policy documents (CODE_OF_CONDUCT.md, CONTRIBUTING.md, SECURITY.md) (#140)

## v6.0.7 (2022-02-11)

### Fixed

- Update to latest dependencies, including resolving `follow-redirects` vulnerability CVE-2022-0536

## v6.0.6 (2022-01-26)

### Fixed

- Updated to latest dependencies, including resolving `node-fetch` vulnerability CVE-2022-0235

## v6.0.5 (2022-01-19)

### Miscellaneous

- Added reporter `pa11y-ci-reporter-cli-summary` to `pa11y-ci` job (#139)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v6.0.4 (2021-12-18)

### Fixed

- Updated to latest dependencies, including `puppeteer` v13 with Chromium 97

## v6.0.3 (2021-11-18)

### Fixed

- Fixed issue with `secret_detection` job failing in some cases (#136)
- Updated to latest dependencies, including latest Node 16 image

## v6.0.2 (2021-11-17)

### Fixed

- Updated to latest dependencies, including latest Node 16 image

## v6.0.1 (2021-11-11)

### Fixed

- Fixed formatting of HTML report to wrap ling lines in test error/warning data and highlight background. (#134, #135)
- Updated to latest dependencies

## v6.0.0 (2021-11-07)

### **Security Notice**

- Pagean leverages [HTMLHint](https://github.com/htmlhint/HTMLHint) for the Rendered HTML Test. One of its child dependencies, `glob-parent`, includes a regular expression denial of service (ReDoS) vulnerability, tracked as [CVE-2020-28469](https://nvd.nist.gov/vuln/detail/CVE-2020-28469), [CWE-400](https://ossindex.sonatype.org/vulnerability/64cd5f21-8af4-4eae-ac7d-a53241ea693a), and a [GitHub Advisory](https://github.com/advisories/GHSA-ww39-953v-wcq6). This vulnerability has been assessed and only affects the CLI interface to HTMLHint. Pagean only uses the programmatic API for HTMLHint and is not susceptible to this vulnerability, even though it is identified through various scanners simply based on the included dependency. Resolution of the issue is tracked in this [Pagean issue](https://gitlab.com/gitlab-ci-utils/pagean/-/issues/118).

### Changed

- BREAKING: Changed exit code to 2 for any Pagean test error to differentiate from a test execution error, which will still result in exit code 1. (#125)
- BREAKING: Updated broken links test to cache results and not re-test duplicate links. Added configuration option `ignoreDuplicates` that can be used to bypass this behavior. (#112)
- BREAKING: Updated docker image to Node 16, which is now the active LTS release (#127)
- Added `checkWithBrowser` configuration option for the Broken Link Test that test links by loading them in the browser to address some false failure cases. Note this can increase test execution time, in some cases substantially, due to the time to open a new browser tab and plus load the page and all assets. (#105)

### Fixed

- Updated to latest dependencies (#102)
- Updated rules for scheduled pipelines to build from correct `Dockerfile`, and only run required jobs for security scans and other schedule-only checks. (#124, #133)
- Refactored tests for per updated lint rules

### Miscellaneous

- Configured [renovate](https://docs.renovatebot.com/) for dependency updates (#119, #131)
- Update test suite to include node 17 (!149, #126, #130)
- Moved to container build/test/deploy pipeline leveraging `kaniko` for build and `skopeo` for deploy. (#121)
- Updated project CSS and HTML lint configurations, with associated fixes (#128, #129)

## v5.0.0 (2021-07-25)

**See the [version upgrade guide](https://gitlab.com/gitlab-ci-utils/pagean/-/blob/master/docs/upgrade-guide.md) for details on breaking changes and instructions on upgrading from v4.x to v5.**

### Changed

- BREAKING: Changed Broken Link Test default to fail (without warning) to be consistent with other tests. (#83)
- BREAKING: Deprecated support for Node 10 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#108, #109)
- Updated Broken Link Test to ignore HTTPS errors if set in puppeteer launch options (#113)

### Fixed

- Updated error details in HTML report to show different arrows for open/closed state to match the default browser behavior (#114)
- Updated to latest dependencies
  - Resolved several vulnerabilities
  - Updated to [`htmlhint` v0.15.1](https://github.com/htmlhint/HTMLHint)
  - Update code to conform to latest `eslint` rules
- Updated Bootstrap link in test cases (#116)

### Miscellaneous

- Added note on known issues with broken link test, planned to be fixed in a future release (#84, #88)
- Optimize published package to only include the minimum required files (#104)
- Added Node v16 tests (#106)
- Updated CI pipeline for compatibility with the latest CI templates (#110, #111)

## v4.4.3 (2021-03-28)

### Fixed

- Resolved issues with `husky` trying to install in container (#99)
- Updated to latest dependencies (#102)

### Miscellaneous

- Updated CI pipeline to use needs for efficiency (#100)

## v4.4.2 (2021-03-21)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities and puppeteer v8 (#98)

### Miscellaneous

- Updated CI pipeline with multiple optimizations:
  - Changed `merge_request` pipelines to run instead of `push` pipelines when in a merge request
  - Moved all jobs from `only`/`except` to `rules` (#92)
  - Optimized pipeline for schedules (#93) and to use standard NPM package collection (#95)
  - Updated Pagean jobs to be included in release evidence collection (#96)
  - Updated Pagean jobs for release evidence collection (#96)
  - Updated Docker build to ensure latest published version is installed in tag pipelines (#89)
  - Refactor Docker build using rules to reduce to a single job, closes #94
  - Update Docker build rules for local vs npm installation (#34)
- Update to @aarongoldenthal/stylelint-config-standard (#91)

## v4.4.1 (2021-01-30)

### Fixed

- Fixed failure of global pagean install in container (#87)

## v4.4.0 (2021-01-30)

### Added

- Added test to check for broken links on the page (`http`/`file` only). For links within the page, checks for existence of the element on the page. For links to other pages, makes a request and checks response (fails if >= 400 or other error). Includes config update with array of links to ignore. Fails with warning by default so it is not a breaking change. (#77)
- Added [`http-server`](https://www.npmjs.com/package/http-server) and [`wait-on`](https://www.npmjs.com/package/wait-on) to Docker images and documented example showing how to test static files with a local server. (#59)

### Fixed

- Fixed error in console log reporting location information and updated tests (#85, #86)
- Fixed local installation on non-master branch Docker images to accept CLI arguments (#81)
- Updated to latest dependencies

### Miscellaneous

- Update CI pipeline to check for secure JSON schemas (#78) and lint `.pageanrc` files (#79)
- Fix test remnant left from adding `htmlhintrc` (#80)

## v4.3.0 (2020-12-28)

### Added

- Added CLI command to lint pageanrc configuration files (#74)
- Added configuration file property to specify project name for reports (#73)
- Added configuration file property to specify htmlhintrc file (#35)

### Changed

- Improved configuration file validation leveraging JSON schema (#75)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Refactored code to leverage array.map for optimization in several places (#70)

## v4.2.0 (2020-12-13)

### Added

- Added CLI option to specify configuration file location (#14)
- Added configuration file section to specify the reporters used to capture the test results (any or all of `cli`, `html`, or `json`) (#25)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Update test suite to include CLI integration tests (#63)
- Update pipeline for Node LTS change from v12 to v14 (#65)
- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#67) and GitLab Releaser (#68)

## v4.1.3 (2020-10-23)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Added tests with Node 15 (#64)

## v4.1.2 (2020-09-13)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v4.1.1 (2020-08-16)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v4.1.0 (2020-07-05)

### Changed

- Updated console output to improve visualization of test results (#61)

### Fixed

- Updated to latest dependencies, including Puppeteer 5

## v4.0.0 (2020-06-13)

**Note: As of v4.0.0, this module is being being released as [`pagean`](https://www.npmjs.com/package/pagean) and [`page-load-tests`](https://www.npmjs.com/package/page-load-tests) has been deprecated. See details and impacts below. This drove breaking configuration changes that will require updating your project's configuration file. See the [version upgrade guide](https://gitlab.com/gitlab-ci-utils/pagean/-/blob/master/docs/upgrade-guide.md) for complete details.**

### Added

- Added external script test to identify any externally loaded javascript files (e.g. loaded from a CDN) and aggregate those files so they can undergo further analysis (e.g. dependency vulnerability scanning). (#42)
- Added the ability to set a failed test to result in a warning instead of a failure, which will not cause failure of the test suite. (#44)

### Changed

- BREAKING: Updated test configuration to allow test-specific settings. With this change, `pageLoadTimeThreshold` must now appears as a setting of the `pageLoadTimeTest` test. (#54)
- BREAKING: Removed support for Node v13 since now end-of-life. (#56)
- BREAKING: Updated module name to `pagean`, including the following configuration and report name changes (#57):
  - Changed configurations file name from `.pltconfig.json` to `.pageanrc.json`
  - Changed report names from `page-load-test-results.*` to `pagean-results.*`.

### Fixed

- Updated to latest dependencies

## v3.0.0 (2020-04-20)

### Changed

- BREAKING: Removed [Jest](https://jestjs.io/) as the test runner and replaced with new a dedicated test runner optimized for these types of tests. This includes simplified HTML and JSON reports, removing code stack traces on error and replacing with more meaningful data, etc (#43, #48, #33, #49). This also enables future enhancements that a typical test framework could not support (e.g. #47, #44, #42).

### Fixed

- Updated to Puppeteer 3.0.0 with Chrome 81 (#41, #50)
- Updated to latest dependencies to resolve vulnerabilities

## v2.0.0 (2020-01-26)

### Changed

- BREAKING: Removed Node v8 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=13.0.0`) (#39)
- Updated documentation with link to [example report](https://gitlab-ci-utils.gitlab.io/pagean/pagean-results.html) (#28)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added testing for all supported Node.js versions (#36)
- Added accessibility analysis for report via [pa11y-ci](https://www.npmjs.com/package/pa11y-ci) (#38)
- Updated project to use [eslint-plugin-sonarjs](https://www.npmjs.com/package/eslint-plugin-sonarjs) (#37)

## v1.2.1 (2019-11-24)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.0 (2019-11-16)

### Changed

- Updated rendered HTML test to use local `.htmlhintrc` file if available (#17)

## v1.1.1 (2019-11-10)

### Fixed

- Updated tests to exit with error (i.e. non-zero) for test failure (#32, #31)
- Updated to latest dependencies (Puppeteer, eslint/jest/plugins) (#30)

### Miscellaneous

- Updated project to use custom eslint configuration module (#29)
- Updated CI pipeline to lint HTML and CSS

## v1.1.0 (2019-10-19)

### Changed

- Updated tests to show original URLs (without full file path) (#18)
- Added custom HTML report template with error formatting (#10)
- Updated Dockerfile to run via standard CLI command (#21)

## v1.0.1 (2019-10-17)

### Fixed

- Fixed error when running in a path with spaces (#26)

## v1.0.0 (2019-10-16)

Initial release
