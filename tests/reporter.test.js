'use strict';

const fs = require('fs');
const path = require('path');

const reporter = require('../lib/reporter');
const defaultReporters = require('../lib/default-config.json').reporters;

const htmlReportFileName = './pagean-results.html';
const jsonReportRawFileName = 'pagean-results.json';
const jsonReportFileName = `./${jsonReportRawFileName}`;

// eslint-disable-next-line max-lines-per-function
describe('reporter', () => {
    const saveReportTest = (fileName, reporters, saveFile = true) => {
        const testResults = {};
        const writeFileSpy = jest
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});

        reporter.saveReports(testResults, reporters);

        if (saveFile) {
            expect(writeFileSpy).toHaveBeenCalledWith(
                fileName,
                expect.any(String)
            );
        } else {
            expect(writeFileSpy).not.toHaveBeenCalledWith(
                fileName,
                expect.any(String)
            );
        }
        return writeFileSpy.mock.calls[0][1];
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should save HTML report file if specified in reporters', () => {
        expect.assertions(1);
        const reporters = ['html'];
        saveReportTest(htmlReportFileName, reporters, true);
    });

    it('should not save HTML report file if not specified in reporters', () => {
        expect.assertions(1);
        const reporters = ['json'];
        saveReportTest(htmlReportFileName, reporters, false);
    });

    it('should save JSON report file if specified in reporters', () => {
        expect.assertions(1);
        const reporters = ['json'];
        saveReportTest(jsonReportFileName, reporters, true);
    });

    it('should not save JSON report file if not specified in reporters', () => {
        expect.assertions(1);
        const reporters = ['html'];
        saveReportTest(jsonReportFileName, reporters, false);
    });

    it('should save JSON report with valid JSON', () => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        const reportContents = saveReportTest(
            jsonReportFileName,
            defaultReporters
        );
        expect(() => JSON.parse(reportContents)).not.toThrow();
    });

    it('should write expected JSON and HTML reports', () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        const reportCount = 2;
        const jsonReportFile = path.join(
            'tests',
            'test-cases',
            jsonReportRawFileName
        );
        const testResults = JSON.parse(fs.readFileSync(jsonReportFile, 'utf8'));
        const writeFileSpy = jest
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});

        reporter.saveReports(testResults, defaultReporters);

        expect(writeFileSpy).toHaveBeenCalledTimes(reportCount);
        // eslint-disable-next-line prefer-destructuring -- less intuitive
        const jsonReport = writeFileSpy.mock.calls[0][1];
        expect(jsonReport).toMatchSnapshot('json report');

        // eslint-disable-next-line prefer-destructuring -- less intuitive
        const htmlReport = writeFileSpy.mock.calls[1][1];
        expect(htmlReport).toMatchSnapshot('html report');
    });
});
