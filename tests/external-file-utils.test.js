'use strict';

const fs = require('fs');
const path = require('path');

const axios = require('axios');

const {
    shouldSaveFile,
    saveExternalScript
} = require('../lib/external-file-utils');

// eslint-disable-next-line max-lines-per-function
describe('shouldSaveFile', () => {
    const urlBaseWithHost = 'https://foo.com/';
    const urlBaseWithoutHost = 'file:///E:/this/is/a/test/';
    const scriptFileName = 'jquery-3.5.1.min.js';
    const pageFileName = 'index.html';
    const invalidUrl = 'foo';

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should return false for script and page with same host', () => {
        expect.assertions(1);
        const script = `${urlBaseWithHost}${scriptFileName}`;
        const page = urlBaseWithHost;
        expect(shouldSaveFile(script, page)).toBe(false);
    });

    it('should return false for script and page with same host and different protocols', () => {
        expect.assertions(1);
        const script = `${urlBaseWithHost}${scriptFileName}`;
        const page = 'file://foo.com/';
        expect(shouldSaveFile(script, page)).toBe(false);
    });

    it('should return true for script and page with different hosts', () => {
        expect.assertions(1);
        const script = `${urlBaseWithHost}${scriptFileName}`;
        const page = `https://bar.com/${pageFileName}`;
        expect(shouldSaveFile(script, page)).toBe(true);
    });

    it('should return true for script with no host and page with host', () => {
        expect.assertions(1);
        const script = `${urlBaseWithoutHost}${scriptFileName}`;
        const page = urlBaseWithHost;
        expect(shouldSaveFile(script, page)).toBe(true);
    });

    it('should return true for script with host and page with no host', () => {
        expect.assertions(1);
        const script = `${urlBaseWithHost}${scriptFileName}`;
        const page = `${urlBaseWithoutHost}${pageFileName}`;
        expect(shouldSaveFile(script, page)).toBe(true);
    });

    it('should return false for script and page with no host', () => {
        expect.assertions(1);
        const script = `${urlBaseWithoutHost}${scriptFileName}`;
        const page = `${urlBaseWithoutHost}${pageFileName}`;
        expect(shouldSaveFile(script, page)).toBe(false);
    });

    it('should throw for invalid script URL', () => {
        expect.assertions(1);
        const script = invalidUrl;
        const page = urlBaseWithHost;
        expect(() => shouldSaveFile(script, page)).toThrow('Invalid URL');
    });

    it('should throw for invalid page URL', () => {
        expect.assertions(1);
        const script = `${urlBaseWithoutHost}${scriptFileName}`;
        const page = invalidUrl;
        expect(() => shouldSaveFile(script, page)).toThrow('Invalid URL');
    });
});

// eslint-disable-next-line max-lines-per-function
describe('saveExternalScript', () => {
    const jqueryUrl = 'https://code.jquery.com/jquery-3.5.1.slim.min.js';
    const jqueryPath = path.join(
        'pagean-external-scripts',
        'code.jquery.com',
        'jquery-3.5.1.slim.min.js'
    );
    const scriptText = 'this is a script';
    const httpResponseOk = 200;
    const httpResponseNotFound = 404;

    afterEach(() => {
        jest.restoreAllMocks();
    });

    const saveExternalScriptWithSpies = async (
        url,
        status = httpResponseOk,
        data = scriptText,
        fileExists = false
    ) => {
        const axiosSpy = jest.spyOn(axios, 'get').mockImplementation(() => {
            if (status === httpResponseOk) {
                return { status, data };
            }
            throw new Error(`Request failed with status code ${status}`);
        });
        const existsSpy = jest
            .spyOn(fs, 'existsSync')
            .mockImplementation(() => fileExists);
        const mkdirSpy = jest
            .spyOn(fs, 'mkdirSync')
            .mockImplementation(() => {});
        const writeFileSpy = jest
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});
        const result = await saveExternalScript(url);
        return {
            result,
            axiosSpy,
            existsSpy,
            mkdirSpy,
            writeFileSpy
        };
    };

    it('should try to get script if file does not exist', async () => {
        expect.assertions(1);
        const { axiosSpy } = await saveExternalScriptWithSpies(jqueryUrl);
        expect(axiosSpy).toHaveBeenCalledWith(jqueryUrl);
    });

    it('should not try to get script, or make dir, or save file if file exists', async () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        const { axiosSpy, mkdirSpy, writeFileSpy } =
            await saveExternalScriptWithSpies(
                jqueryUrl,
                httpResponseOk,
                scriptText,
                true
            );
        expect(axiosSpy).not.toHaveBeenCalled();
        expect(mkdirSpy).not.toHaveBeenCalled();
        expect(writeFileSpy).not.toHaveBeenCalled();
    });

    it('should create output directory if script is retrieved successfully', async () => {
        expect.assertions(1);
        const { mkdirSpy } = await saveExternalScriptWithSpies(jqueryUrl);
        expect(mkdirSpy).toHaveBeenCalledWith(
            path.dirname(jqueryPath),
            expect.objectContaining({ recursive: true })
        );
    });

    it('should save file if script is retrieved successfully', async () => {
        expect.assertions(1);
        const { writeFileSpy } = await saveExternalScriptWithSpies(
            jqueryUrl,
            httpResponseOk,
            scriptText
        );
        expect(writeFileSpy).toHaveBeenCalledWith(jqueryPath, scriptText);
    });

    it('should return script URL and saved file name if script is retrieved successfully', async () => {
        expect.assertions(1);
        const { result } = await saveExternalScriptWithSpies(
            jqueryUrl,
            httpResponseOk,
            scriptText
        );
        expect(result).toStrictEqual(
            expect.objectContaining({
                url: jqueryUrl,
                localFile: jqueryPath
            })
        );
    });

    it('should return script URL and saved file name if script already existed', async () => {
        expect.assertions(1);
        const { result } = await saveExternalScriptWithSpies(
            jqueryUrl,
            httpResponseOk,
            scriptText,
            true
        );
        expect(result).toStrictEqual(
            expect.objectContaining({
                url: jqueryUrl,
                localFile: jqueryPath
            })
        );
    });

    it('should not create output directory if script is not retrieved successfully', async () => {
        expect.assertions(1);
        const { mkdirSpy } = await saveExternalScriptWithSpies(
            jqueryUrl,
            httpResponseNotFound
        );
        expect(mkdirSpy).not.toHaveBeenCalledWith();
    });

    it('should not save file if script is not retrieved successfully', async () => {
        expect.assertions(1);
        const { writeFileSpy } = await saveExternalScriptWithSpies(
            jqueryUrl,
            httpResponseNotFound
        );
        expect(writeFileSpy).not.toHaveBeenCalledWith();
    });

    it('should return script URL and error message if script is is retrieved successfully', async () => {
        expect.assertions(1);
        const { result } = await saveExternalScriptWithSpies(
            jqueryUrl,
            httpResponseNotFound,
            scriptText
        );
        expect(result).toStrictEqual(
            expect.objectContaining({
                url: jqueryUrl,
                error: expect.stringContaining(String(httpResponseNotFound))
            })
        );
    });

    it('should retrieve script that matches snapshot', async () => {
        expect.assertions(1);
        // Add fs spies to avoid actually writing the file
        jest.spyOn(fs, 'mkdirSync').mockImplementation(() => {});
        const writeFileSpy = jest
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});
        await saveExternalScript(jqueryUrl);
        expect(writeFileSpy.mock.calls[0][1]).toMatchSnapshot();
    });
});
