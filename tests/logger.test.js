/* eslint-disable max-lines */

'use strict';

const testLogger = require('../lib/logger');
const { testResultStates } = require('../lib/test-utils');
const defaultConfig = require('../lib/default-config.json');
defaultConfig.project = 'Default project';

const configNoCli = {
    reporters: ['html', 'json']
};

const defaultUrl = 'https://somewhere.com';
const defaultTestName = 'should run a test';

// eslint-disable-next-line max-lines-per-function
describe('startUrlTests', () => {
    let loggerWithCli, loggerNoCli;

    beforeEach(() => {
        loggerWithCli = testLogger(defaultConfig);
        loggerNoCli = testLogger(configNoCli);
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should add URL results object to test results', () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        // Console log is mocked to avoid writing output to the console during the test
        jest.spyOn(console, 'log').mockImplementation(() => {});

        const startingResults = loggerWithCli.getTestResults();
        expect(startingResults.results).toHaveLength(0);

        loggerWithCli.startUrlTests(defaultUrl);
        const endingResults = loggerWithCli.getTestResults();
        expect(endingResults.results).toHaveLength(1);
        expect(endingResults.results[0].url).toBe(defaultUrl);
    });

    it('should log to console with URL being tested if CLI reporter enabled', () => {
        expect.assertions(1);
        const consoleLogSpy = jest
            .spyOn(console, 'log')
            .mockImplementation(() => {});
        loggerWithCli.startUrlTests(defaultUrl);
        expect(consoleLogSpy).toHaveBeenCalledWith(
            expect.stringContaining(defaultUrl)
        );
    });

    it('should not log to console with URL being tested if CLI reporter disabled', () => {
        expect.assertions(1);
        const consoleLogSpy = jest
            .spyOn(console, 'log')
            .mockImplementation(() => {});
        loggerNoCli.startUrlTests(defaultUrl);
        expect(consoleLogSpy).not.toHaveBeenCalled();
    });
});

// eslint-disable-next-line max-lines-per-function
describe('logTestResults', () => {
    let loggerWithCli, loggerNoCli;

    beforeEach(() => {
        loggerWithCli = testLogger(defaultConfig);
        loggerNoCli = testLogger(configNoCli);
    });

    afterEach(() => {
        jest.restoreAllMocks();
        delete require.cache[require.resolve('../lib/logger')];
    });

    // This includes only the console log from startUrlTests and getTestResults
    const consoleLoggerSpyDefaultCalls = 2;
    const testLogTestResults = (testResults, logger) => {
        const consoleLogSpy = jest
            .spyOn(console, 'log')
            .mockImplementation(() => {});

        logger.startUrlTests(defaultUrl);
        logger.logTestResults(testResults);

        const allTestResults = logger.getTestResults();

        return { allTestResults, consoleLogSpy };
    };

    const testResultSymbols = Object.freeze({
        passed: '√',
        failed: '×',
        warning: '‼'
    });

    const testConsoleLogOutput = (testResult, logger) => {
        const consoleSpy = jest
            .spyOn(console, 'log')
            .mockImplementation(() => {});

        logger.startUrlTests(defaultUrl);

        const testResults = {
            name: defaultTestName,
            result: testResult,
            data: []
        };
        logger.logTestResults(testResults);
        return consoleSpy;
    };

    it('should not log test results if no results provided', () => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        const { allTestResults, consoleLogSpy } = testLogTestResults(
            undefined,
            loggerWithCli
        );
        expect(allTestResults.results[0].tests).toHaveLength(0);
        expect(consoleLogSpy).toHaveBeenCalledTimes(
            consoleLoggerSpyDefaultCalls
        );
    });

    it('should log test results if provided', () => {
        expect.assertions(1);
        const testResults = {
            name: defaultTestName,
            result: testResultStates.passed,
            data: []
        };
        const { allTestResults } = testLogTestResults(
            testResults,
            loggerWithCli
        );
        expect(allTestResults.results[0].tests[0]).toBe(testResults);
    });

    it('should output test name to console if test results provided and CLI reporter enabled', () => {
        expect.assertions(1);
        const consoleSpy = testConsoleLogOutput(
            testResultStates.passed,
            loggerWithCli
        );

        expect(consoleSpy).toHaveBeenNthCalledWith(
            // eslint-disable-next-line no-magic-numbers -- index
            2,
            expect.stringContaining(defaultTestName)
        );
    });

    it('should not output test name to console if test results provided and CLI reporter disabled', () => {
        expect.assertions(1);
        const consoleSpy = testConsoleLogOutput(
            testResultStates.passed,
            loggerNoCli
        );
        expect(consoleSpy).not.toHaveBeenCalled();
    });

    const testConsoleLogTestResults = (testResult) => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        const consoleSpy = testConsoleLogOutput(testResult, loggerWithCli);
        /* eslint-disable no-magic-numbers */
        expect(consoleSpy).toHaveBeenNthCalledWith(
            2,
            expect.stringContaining(testResult)
        );
        expect(consoleSpy).toHaveBeenNthCalledWith(
            2,
            expect.stringContaining(testResultSymbols[testResult])
        );
        /* eslint-enable no-magic-numbers */
    };

    const testNoConsoleLogTestResults = (testResult) => {
        expect.assertions(1);
        const consoleSpy = testConsoleLogOutput(testResult, loggerNoCli);
        expect(consoleSpy).not.toHaveBeenCalled();
    };

    it('should output test results to console for a passed test if CLI reporter enabled', () => {
        expect.hasAssertions();
        testConsoleLogTestResults(testResultStates.passed);
    });

    it('should not output test results to console for a passed test if CLI reporter disabled', () => {
        expect.hasAssertions();
        testNoConsoleLogTestResults(testResultStates.passed);
    });

    it('should output test results to console for a warning test if CLI reporter enabled', () => {
        expect.hasAssertions();
        testConsoleLogTestResults(testResultStates.warning);
    });

    it('should not output test results to console for a warning test if CLI reporter disabled', () => {
        expect.hasAssertions();
        testNoConsoleLogTestResults(testResultStates.warning);
    });

    it('should output test results to console for a failed test if CLI reporter enabled', () => {
        expect.hasAssertions();
        testConsoleLogTestResults(testResultStates.failed);
    });

    it('should not output test results to console for a failed test if CLI reporter disabled', () => {
        expect.hasAssertions();
        testNoConsoleLogTestResults(testResultStates.failed);
    });

    it('should increment only test and passed counters for a passed test', () => {
        expect.assertions(4); // eslint-disable-line no-magic-numbers
        const testResults = {
            name: defaultTestName,
            result: testResultStates.passed
        };
        const { allTestResults } = testLogTestResults(
            testResults,
            loggerWithCli
        );
        expect(allTestResults.summary.tests).toBe(1);
        expect(allTestResults.summary.passed).toBe(1);
        expect(allTestResults.summary.warning).toBe(0);
        expect(allTestResults.summary.failed).toBe(0);
    });

    it('should increment only test and failed counters for a failed test', () => {
        expect.assertions(4); // eslint-disable-line no-magic-numbers
        const testResults = {
            name: defaultTestName,
            result: testResultStates.failed
        };
        const { allTestResults } = testLogTestResults(
            testResults,
            loggerWithCli
        );
        expect(allTestResults.summary.tests).toBe(1);
        expect(allTestResults.summary.passed).toBe(0);
        expect(allTestResults.summary.warning).toBe(0);
        expect(allTestResults.summary.failed).toBe(1);
    });

    it('should increment only test and warning counters for a warning test', () => {
        expect.assertions(4); // eslint-disable-line no-magic-numbers
        const testResults = {
            name: defaultTestName,
            result: testResultStates.warning
        };
        const { allTestResults } = testLogTestResults(
            testResults,
            loggerWithCli
        );
        expect(allTestResults.summary.tests).toBe(1);
        expect(allTestResults.summary.passed).toBe(0);
        expect(allTestResults.summary.warning).toBe(1);
        expect(allTestResults.summary.failed).toBe(0);
    });
});

// eslint-disable-next-line max-lines-per-function
describe('getTestResults', () => {
    let loggerWithCli, loggerNoCli;

    beforeEach(() => {
        loggerWithCli = testLogger(defaultConfig);
        loggerNoCli = testLogger(configNoCli);
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    const testGetTestResults = (includeTests, testName, testResult, logger) => {
        const consoleLogSpy = jest
            .spyOn(console, 'log')
            .mockImplementation(() => {});

        if (includeTests) {
            logger.startUrlTests(defaultUrl);
            const name = testName;
            const result = testResult;
            logger.logTestResults({ name, result });
        }

        const results = logger.getTestResults();
        return {
            results,
            consoleLogSpy
        };
    };

    it('should return a results object with appropriate properties with no tests run', () => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers

        const { results } = testGetTestResults(
            false,
            undefined,
            undefined,
            loggerWithCli
        );
        expect(results).toStrictEqual(
            expect.objectContaining({
                project: defaultConfig.project,
                executionStart: expect.any(Date),
                summary: {
                    tests: 0,
                    passed: 0,
                    warning: 0,
                    failed: 0
                },
                results: expect.any(Array)
            })
        );
        expect(results.results).toHaveLength(0);
    });

    // eslint-disable-next-line max-lines-per-function
    it('should return a results object with appropriate properties with tests run', () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers

        const testName = defaultTestName;
        const testResult = testResultStates.passed;
        const { results } = testGetTestResults(
            true,
            testName,
            testResult,
            loggerWithCli
        );

        expect(results).toStrictEqual(
            expect.objectContaining({
                project: defaultConfig.project,
                executionStart: expect.any(Date),
                summary: {
                    tests: 1,
                    passed: 1,
                    warning: 0,
                    failed: 0
                },
                results: expect.any(Array)
            })
        );
        expect(results.results).toHaveLength(1);
        expect(results.results[0]).toStrictEqual(
            expect.objectContaining({
                url: defaultUrl,
                tests: [{ name: testName, result: testResult }]
            })
        );
    });

    const testTestResultsSummary = (logger, messageCount) => {
        expect.assertions(4); // eslint-disable-line no-magic-numbers

        const testResult = testResultStates.warning;
        const { consoleLogSpy } = testGetTestResults(
            true,
            defaultTestName,
            testResult,
            logger
        );

        // eslint-disable-next-line prefer-destructuring
        const message = consoleLogSpy.mock.calls[messageCount - 1][0];
        expect(message).toMatch(/Tests: 1/);
        expect(message).toMatch(/Passed: 0/);
        expect(message).toMatch(/Warning: 1/);
        expect(message).toMatch(/Failed: 0/);
    };

    it('should log to console the test result summary with total, passed, failed, and warning counts if CLI reporter enabled', () => {
        expect.hasAssertions();
        // Expect test name, test result, and the final results messages
        const messageCount = 3;
        testTestResultsSummary(loggerWithCli, messageCount);
    });

    it('should log to console the test result summary with total, passed, failed, and warning counts if CLI reporter disabled', () => {
        expect.hasAssertions();
        // Expect only the final results message
        const messageCount = 1;
        testTestResultsSummary(loggerNoCli, messageCount);
    });
});
