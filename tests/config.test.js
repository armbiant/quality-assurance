/* eslint-disable max-lines */
'use strict';

const fs = require('fs');
const config = require('../lib/config');
const defaultConfig = require('../lib/default-config.json');

const nonExistentConfigFileName = 'not-a-file.json';
const invalidConfigFileName =
    './tests/test-configs/unit-tests/invalid.pageanrc.json';
const emptyConfigFileName =
    './tests/test-configs/unit-tests/empty.pageanrc.json';
const emptyUrlsConfigFileName =
    './tests/test-configs/unit-tests/empty-urls.pageanrc.json';
const emptyUrlValuesConfigFileName =
    './tests/test-configs/unit-tests/empty-url-values.pageanrc.json';
const noTestSettingsConfigFileName =
    './tests/test-configs/unit-tests/no-test-settings.pageanrc.json';
const globalTestSettingsConfigFileName =
    './tests/test-configs/unit-tests/global-test-settings.pageanrc.json';
const testSpecificSettingsConfigFileName =
    './tests/test-configs/unit-tests/test-specific-settings.pageanrc.json';
const globalAndTestSpecificSettingsConfigFileName =
    './tests/test-configs/unit-tests/global-and-test-specific-settings.pageanrc.json';
const projectNoTestSettingsConfigFileName =
    './tests/test-configs/unit-tests/project-no-test-settings.pageanrc.json';
const puppeteerNoTestSettingsConfigFileName =
    './tests/test-configs/unit-tests/puppeteer-no-test-settings.pageanrc.json';
const urlTypesConfigFileName =
    './tests/test-configs/unit-tests/url-types.pageanrc.json';
const allTestPropertiesConfigFileName =
    './tests/test-configs/unit-tests/global-and-test-specific-settings-test-props.pageanrc.json';
const allShorthandConfigFileName =
    './tests/test-configs/unit-tests/global-and-test-specific-settings-shorthand.pageanrc.json';
const reportersEmptyArrayConfigFileName =
    './tests/test-configs/unit-tests/reporters-empty.pageanrc.json';
const reportersInvalidTypeConfigFileName =
    './tests/test-configs/unit-tests/reporters-invalid-type.pageanrc.json';
const reportersNotArrayConfigFileName =
    './tests/test-configs/unit-tests/reporters-not-array.pageanrc.json';
const reportersValidConfigFileName =
    './tests/test-configs/unit-tests/reporters-valid.pageanrc.json';
const htmlhintrcInvalidConfigFileName =
    './tests/test-configs/unit-tests/htmlhintrc-invalid.pageanrc.json';
const ignoredLinksDenormalizedConfigFileName =
    './tests/test-configs/unit-tests/ignored-links-denormalized.pageanrc.json';
const htmlhintrcValidConfigFileName =
    './tests/test-configs/unit-tests/htmlhintrc-valid.pageanrc.json';
const cliDefaultConfigFileName =
    './tests/test-configs/cli-tests/.pageanrc.json';

// eslint-disable-next-line max-lines-per-function
describe('process config', () => {
    // eslint-disable-next-line max-lines-per-function
    describe('invalid config', () => {
        const invalidConfigMessage = 'invalid pageanrc schema';

        it('should throw if config file not found', () => {
            expect.assertions(1);
            expect(() => config(nonExistentConfigFileName)).toThrow('ENOENT');
        });

        it('should throw if config file is not JSON', () => {
            expect.assertions(1);
            expect(() => config(invalidConfigFileName)).toThrow(
                'Unexpected end of JSON input'
            );
        });

        it('should throw if no URLs specified in config file', () => {
            expect.assertions(1);
            expect(() => config(emptyConfigFileName)).toThrow(
                invalidConfigMessage
            );
        });

        it('should throw if empty URL array specified in config file', () => {
            expect.assertions(1);
            expect(() => config(emptyUrlsConfigFileName)).toThrow(
                invalidConfigMessage
            );
        });

        it('should throw if URL object with no url property specified in config file', () => {
            expect.assertions(1);
            expect(() => config(emptyUrlValuesConfigFileName)).toThrow(
                invalidConfigMessage
            );
        });

        it('should throw if reporters specified in config are not array', () => {
            expect.assertions(1);
            expect(() => config(reportersNotArrayConfigFileName)).toThrow(
                invalidConfigMessage
            );
        });

        it('should throw if reporters specified in config are empty array', () => {
            expect.assertions(1);
            expect(() => config(reportersEmptyArrayConfigFileName)).toThrow(
                invalidConfigMessage
            );
        });

        it('should throw if reporters array specified in config has invalid values', () => {
            expect.assertions(1);
            expect(() => config(reportersInvalidTypeConfigFileName)).toThrow(
                invalidConfigMessage
            );
        });
    });

    const checkConfigAgainstSnapshot = (configFileName, property) => {
        expect.assertions(1);
        const processedConfig = config(configFileName);
        expect(processedConfig[property]).toMatchSnapshot();
    };

    describe('project name', () => {
        it('should return config with project name if provided', () => {
            expect.assertions(1);
            const processedConfig = config(projectNoTestSettingsConfigFileName);
            expect(processedConfig.project).toBe('This is a test project');
        });

        it('should return config with empty project name if none provided', () => {
            expect.assertions(1);
            const processedConfig = config(
                puppeteerNoTestSettingsConfigFileName
            );
            expect(processedConfig.project).toBe('');
        });
    });

    describe('puppeteer launch options', () => {
        it('should return config with puppeteer launch settings', () => {
            expect.hasAssertions();
            checkConfigAgainstSnapshot(
                puppeteerNoTestSettingsConfigFileName,
                'puppeteerLaunchOptions'
            );
        });

        it('should return config with undefined puppeteer launch settings', () => {
            expect.hasAssertions();
            checkConfigAgainstSnapshot(
                noTestSettingsConfigFileName,
                'puppeteerLaunchOptions'
            );
        });
    });

    // eslint-disable-next-line max-lines-per-function
    describe('test settings', () => {
        it('should return config with default test settings', () => {
            expect.hasAssertions();
            checkConfigAgainstSnapshot(noTestSettingsConfigFileName, 'urls');
        });

        it('should return config with global test settings', () => {
            expect.hasAssertions();
            checkConfigAgainstSnapshot(
                globalTestSettingsConfigFileName,
                'urls'
            );
        });

        it('should return config with default and test-specific settings', () => {
            expect.hasAssertions();
            checkConfigAgainstSnapshot(
                testSpecificSettingsConfigFileName,
                'urls'
            );
        });

        it('should return config with default, global, and test-specific settings', () => {
            expect.hasAssertions();
            checkConfigAgainstSnapshot(
                globalAndTestSpecificSettingsConfigFileName,
                'urls'
            );
        });

        it('should return config with all url types properly parsed', () => {
            expect.assertions(6); // eslint-disable-line no-magic-numbers
            const processedConfig = config(urlTypesConfigFileName);
            // Case 1: Local file that exists.  Use regex since path change in different environments.
            expect(processedConfig.urls[0].url).toMatch(
                /file:\/{3}.*\/tests\/test-cases\/consoleLog.html/
            );
            // Case 2: Local file that does not exist, so assume URL (add https://)
            expect(processedConfig.urls[1].url).toBe(
                'https://tests/test-cases/bar.html'
            );
            // Case 3: URL with no protocol (add https://)
            // eslint-disable-next-line no-magic-numbers -- index
            expect(processedConfig.urls[2].url).toBe(
                'https://localhost/foo.html'
            );
            // Case 4: URL with port and no protocol (add https://)
            // eslint-disable-next-line no-magic-numbers -- index
            expect(processedConfig.urls[3].url).toBe(
                'https://localhost:3000/foo.html'
            );
            // Case 5: URL with http protocol (unchanged)
            // eslint-disable-next-line no-magic-numbers -- index
            expect(processedConfig.urls[4].url).toBe(
                'http://localhost/foo.html'
            );
            // Case 6: URL with https protocol (unchanged)
            // eslint-disable-next-line no-magic-numbers, jest/max-expects -- index, 6 URLs tested
            expect(processedConfig.urls[5].url).toBe(
                'https://localhost/foo.html'
            );
        });

        it('should return the same config with test shorthand and property declaration', () => {
            expect.assertions(1);
            const shorthandConfig = config(allShorthandConfigFileName);
            const testPropertiesConfig = config(
                allTestPropertiesConfigFileName
            );
            expect(shorthandConfig).toStrictEqual(testPropertiesConfig);
        });

        it('should normalize ignored links in brokenLinkTest settings', () => {
            expect.assertions(2); // eslint-disable-line no-magic-numbers
            // Original link: https://this.url.is.ignored/
            const normalizedLinks = ['https://this.url.is.ignored'];
            const globalSettingUrl = 0; // URL inherits global settings
            const testSettingUrl = 1; // URL has test-specific settings
            const normalizedLinksConfig = config(
                ignoredLinksDenormalizedConfigFileName
            );
            expect(
                normalizedLinksConfig.urls[globalSettingUrl].settings
                    .brokenLinkTest.ignoredLinks
            ).toStrictEqual(normalizedLinks);
            expect(
                normalizedLinksConfig.urls[testSettingUrl].settings
                    .brokenLinkTest.ignoredLinks
            ).toStrictEqual(normalizedLinks);
        });
    });

    // eslint-disable-next-line max-lines-per-function
    describe('htmlhint settings', () => {
        const defaultHtmlHintConfigFilename = './.htmlhintrc';
        const invalidHtmlHintConfigFilename = './nonexistent/.htmlhintrc';

        beforeEach(() => {
            jest.restoreAllMocks();
        });

        it('should check for default htmlhintrc file if none specified in config', () => {
            expect.assertions(1);
            const fsExistsSpy = jest
                .spyOn(fs, 'existsSync')
                .mockImplementation(() => false);
            config(globalAndTestSpecificSettingsConfigFileName);
            expect(fsExistsSpy).toHaveBeenCalledWith(
                defaultHtmlHintConfigFilename
            );
        });

        it('should check for htmlhintrc file from config if specified', () => {
            expect.assertions(1);
            const fsExistsSpy = jest
                .spyOn(fs, 'existsSync')
                .mockImplementation(() => false);
            config(htmlhintrcInvalidConfigFileName);
            expect(fsExistsSpy).toHaveBeenCalledWith(
                invalidHtmlHintConfigFilename
            );
        });

        it('should return valid htmlhint settings from file specified in config if file exists', () => {
            expect.hasAssertions();
            checkConfigAgainstSnapshot(
                htmlhintrcValidConfigFileName,
                'htmlHintConfig'
            );
        });

        it('should return valid default htmlhint settings if .htmlhintrc file exists and none specified in config', () => {
            expect.hasAssertions();
            checkConfigAgainstSnapshot(
                globalAndTestSpecificSettingsConfigFileName,
                'htmlHintConfig'
            );
        });

        it('should return no htmlhint config if htmlhintrc file does not exist', () => {
            expect.assertions(1);
            const result = config(htmlhintrcInvalidConfigFileName);
            expect(result.htmlHintConfig).toBeUndefined();
        });
    });

    describe('reporter settings', () => {
        it('should return default reporters if none specified in config', () => {
            expect.assertions(1);
            const processedConfig = config(globalTestSettingsConfigFileName);
            expect(processedConfig).toStrictEqual(
                expect.objectContaining({ reporters: defaultConfig.reporters })
            );
        });

        it('should return reporters from config if valid', () => {
            expect.assertions(1);
            const expectedReporters = ['cli', 'html'];
            const processedConfig = config(reportersValidConfigFileName);
            expect(processedConfig).toStrictEqual(
                expect.objectContaining({ reporters: expectedReporters })
            );
        });
    });
});

// eslint-disable-next-line max-lines-per-function
describe('lint config', () => {
    it('should throw if config file not found', () => {
        expect.assertions(1);
        expect(() => config.lintConfigFile(nonExistentConfigFileName)).toThrow(
            'ENOENT'
        );
    });

    it('should throw if config file is not JSON', () => {
        expect.assertions(1);
        expect(() => config.lintConfigFile(invalidConfigFileName)).toThrow(
            'Unexpected end of JSON input'
        );
    });

    it('should return an object with a boolean isValid property', () => {
        expect.assertions(1);
        const results = config.lintConfigFile(cliDefaultConfigFileName);
        expect(results).toStrictEqual(
            expect.objectContaining({ isValid: expect.any(Boolean) })
        );
    });

    it('should return an object with an errors array', () => {
        expect.assertions(1);
        const results = config.lintConfigFile(cliDefaultConfigFileName);
        expect(results).toStrictEqual(
            expect.objectContaining({ errors: expect.any(Array) })
        );
    });

    it('should return an object with isValid true if schema is valid', () => {
        expect.assertions(1);
        const results = config.lintConfigFile(cliDefaultConfigFileName);
        expect(results.isValid).toBe(true);
    });

    it('should return an object with isValid false if schema is invalid', () => {
        expect.assertions(1);
        const results = config.lintConfigFile(emptyUrlsConfigFileName);
        expect(results.isValid).toBe(false);
    });

    it('should return an object with errors array populated if schema is invalid', () => {
        expect.assertions(1);
        const results = config.lintConfigFile(emptyUrlsConfigFileName);
        expect(results.errors).toMatchSnapshot();
    });
});
