/* eslint-disable max-lines */
'use strict';

const fs = require('fs');

const puppeteer = require('puppeteer');
const protocolify = require('protocolify');

const testLogger = require('../lib/logger');
const {
    horizontalScrollbarTest,
    consoleOutputTest,
    consoleErrorTest,
    renderedHtmlTest,
    pageLoadTimeTest,
    externalScriptTest,
    brokenLinkTest
} = require('../lib/tests');
const { testResultStates } = require('../lib/test-utils');
const fileUtils = require('../lib/external-file-utils');
const linkUtils = require('../lib/link-utils');
const defaultConfig = require('../lib/default-config.json');

let browser, page;

const getPage = async (url) => {
    if (!browser) {
        const puppeteerLaunchOptions = { args: ['--no-sandbox'] };
        browser = await puppeteer.launch(puppeteerLaunchOptions);
    }
    page = await browser.newPage();
    await page.goto(url, { waitUntil: 'load' });
};

// File is read each time instead of require to ensure original,
// not cached or modified cached, values.
const getDefaultSettings = () => {
    return JSON.parse(fs.readFileSync('./lib/default-config.json', 'utf8'))
        .settings;
};

const getContext = async (url, consoleLog, includePage = false) => {
    if (includePage) {
        await getPage(protocolify(url));
    }
    return {
        page,
        consoleLog,
        urlSettings: getDefaultSettings(),
        logger: testLogger(defaultConfig),
        linkChecker: linkUtils.createLinkChecker()
    };
};

const closePageAndBrowser = async () => {
    await page.close();
    page = undefined;
    await browser.close();
    browser = undefined;
};

const consoleLogWithoutErrors = [
    {
        type: 'log',
        text: 'This is a test',
        location: {
            url: 'file:///E:/Users/agoldent/Documents/Projects/NodeJS/Page-Load-Tests/tests/test-cases/consoleLog.html',
            lineNumber: 7,
            columnNumber: 16
        }
    }
];

const consoleLogWithErrors = [
    {
        type: 'error',
        text: 'Failed to load resource: net::ERR_NAME_NOT_RESOLVED',
        location: {
            url: 'https://this.url.does.not.exist/file.js'
        }
    }
];

const executeTestWithLoggerSpy = async (
    context,
    testFunction,
    checkLinkResult = linkUtils.httpResponse.ok
) => {
    const loggerSpy = jest
        .spyOn(context.logger, 'logTestResults')
        .mockImplementation(() => {});
    const saveScriptSpy = jest
        .spyOn(fileUtils, 'saveExternalScript')
        .mockImplementation(() => {});
    const checkLinkSpy = jest
        .spyOn(context.linkChecker, 'checkLink')
        .mockImplementation(() => checkLinkResult);
    await testFunction(context);
    return {
        loggerSpy,
        saveScriptSpy,
        checkLinkSpy
    };
};

// eslint-disable-next-line max-lines-per-function
describe('consoleErrorTest', () => {
    // eslint-disable-next-line require-await
    const testConsoleErrorTest = async (context) => {
        return executeTestWithLoggerSpy(context, consoleErrorTest);
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should not execute consoleErrorTest if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        context.urlSettings.consoleErrorTest.enabled = false;
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result if console log has no entries', async () => {
        expect.assertions(1);
        const context = await getContext('', []);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a passed test result if console log has entries without errors', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithoutErrors);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result if console log has entries with errors', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with console log errors for a failed test', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should not log data with console log errors for a passed test', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithoutErrors);
        const { loggerSpy } = await testConsoleErrorTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });
});

// eslint-disable-next-line max-lines-per-function
describe('consoleOutputTest', () => {
    // eslint-disable-next-line require-await
    const testConsoleOutputTest = async (context) => {
        return executeTestWithLoggerSpy(context, consoleOutputTest);
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should not execute consoleOutputTest if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        context.urlSettings.consoleOutputTest.enabled = false;
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result if console log has no entries', async () => {
        expect.assertions(1);
        const context = await getContext('', []);
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result if console log has entries', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithoutErrors);
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with console log for a failed test', async () => {
        expect.assertions(1);
        const context = await getContext('', consoleLogWithErrors);
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should log not data with console log for a passed test', async () => {
        expect.assertions(1);
        const context = await getContext('', []);
        const { loggerSpy } = await testConsoleOutputTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });
});

// eslint-disable-next-line max-lines-per-function
describe('horizontalScrollbarTest', () => {
    const noHorizontalScrollbarUrl = 'tests/test-cases/consoleLog.html';
    const horizontalScrollbarUrl = 'tests/test-cases/horizontalScrollbar.html';

    // eslint-disable-next-line require-await
    const testHorizontalScrollbarTest = async (context) => {
        return executeTestWithLoggerSpy(context, horizontalScrollbarTest);
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute horizontalScrollbarTest if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(noHorizontalScrollbarUrl, [], true);
        context.urlSettings.horizontalScrollbarTest.enabled = false;
        const { loggerSpy } = await testHorizontalScrollbarTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result for page with no horizontal scroll bar', async () => {
        expect.assertions(1);
        const context = await getContext(noHorizontalScrollbarUrl, [], true);
        const { loggerSpy } = await testHorizontalScrollbarTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result for page with a horizontal scroll bar', async () => {
        expect.assertions(1);
        const context = await getContext(horizontalScrollbarUrl, [], true);
        const { loggerSpy } = await testHorizontalScrollbarTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });
});

// eslint-disable-next-line max-lines-per-function
describe('pageLoadTimeTest', () => {
    const normalLoadTimeUrl = 'tests/test-cases/dynamicContent.html';
    const slowLoadTimeUrl = 'tests/test-cases/slowLoad.html';

    // eslint-disable-next-line require-await
    const testPageLoadTimeTest = async (context) => {
        return executeTestWithLoggerSpy(context, pageLoadTimeTest);
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute pageLoadTimeTest if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(normalLoadTimeUrl, [], true);
        context.urlSettings.pageLoadTimeTest.enabled = false;
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result for page that loads within timeout', async () => {
        expect.assertions(1);
        const context = await getContext(normalLoadTimeUrl, [], true);
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result and data with page load time for page that does not load within timeout', async () => {
        expect.assertions(1);
        const context = await getContext(slowLoadTimeUrl, [], true);
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with page load time for a failed test', async () => {
        expect.assertions(1);
        const context = await getContext(slowLoadTimeUrl, [], true);
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({
                data: { pageLoadTime: expect.any(Number) }
            })
        );
    });

    it('should not log data with page load time for a passed test', async () => {
        expect.assertions(1);
        const context = await getContext(normalLoadTimeUrl, [], true);
        const { loggerSpy } = await testPageLoadTimeTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({
                data: { pageLoadTime: expect.any(Number) }
            })
        );
    });
});

// eslint-disable-next-line max-lines-per-function
describe('renderedHtmlTest', () => {
    const validHtmlUrl = 'tests/test-cases/scriptError404.html';
    const invalidHtmlUrl = 'tests/test-cases/htmlError.html';

    // eslint-disable-next-line require-await
    const testRenderedHtmlTest = async (context) => {
        return executeTestWithLoggerSpy(context, renderedHtmlTest);
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute test if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(validHtmlUrl, [], true);
        context.urlSettings.renderedHtmlTest.enabled = false;
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should log a passed test result for page that has valid HTML', async () => {
        expect.assertions(1);
        const context = await getContext(validHtmlUrl, [], true);
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result for page that has invalid HTML', async () => {
        expect.assertions(1);
        const context = await getContext(invalidHtmlUrl, [], true);
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with HTML errors for a failed test', async () => {
        expect.assertions(1);
        const context = await getContext(invalidHtmlUrl, [], true);
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should not log data with HTML errors for a passed test', async () => {
        expect.assertions(1);
        const context = await getContext(validHtmlUrl, [], true);
        const { loggerSpy } = await testRenderedHtmlTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });
});

// eslint-disable-next-line max-lines-per-function
describe('externalScriptTest', () => {
    const noScriptUrl = 'tests/test-cases/consoleLog.html';
    const noExternalScriptUrl = 'tests/test-cases/noExternalScripts.html';
    const externalScriptUrl = 'tests/test-cases/externalScripts.html';

    // eslint-disable-next-line require-await
    const testExternalScriptTest = async (context) => {
        return executeTestWithLoggerSpy(context, externalScriptTest);
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute test if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(noScriptUrl, [], true);
        context.urlSettings.externalScriptTest.enabled = false;
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should execute test if enabled setting is set to true', async () => {
        expect.assertions(1);
        const context = await getContext(noScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        // Allow since this is just a configuration test to ensure execution, detailed
        // tests follow checking arguments.
        // eslint-disable-next-line jest/prefer-called-with
        expect(loggerSpy).toHaveBeenCalled();
    });

    it('should log a passed test result for page that has no scripts', async () => {
        expect.assertions(1);
        const context = await getContext(noScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a passed test result for page that has no external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(noExternalScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a warning test result for page that has external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(externalScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.warning })
        );
    });

    it('should log data with HTML errors for a page that has external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(externalScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should not log data with HTML errors for a page that does not have external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(noExternalScriptUrl, [], true);
        const { loggerSpy } = await testExternalScriptTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should save files with external scripts for a page that has external scripts', async () => {
        expect.assertions(1);
        const externalFileCount = 4;
        const context = await getContext(externalScriptUrl, [], true);
        const { saveScriptSpy } = await testExternalScriptTest(context);
        expect(saveScriptSpy).toHaveBeenCalledTimes(externalFileCount);
    });

    it('should not save files with external scripts for a page that does not have external scripts', async () => {
        expect.assertions(1);
        const context = await getContext(noExternalScriptUrl, [], true);
        const { saveScriptSpy } = await testExternalScriptTest(context);
        expect(saveScriptSpy).not.toHaveBeenCalled();
    });
});

// eslint-disable-next-line max-lines-per-function
describe('brokenLinkTest', () => {
    const noLinkUrl = 'tests/test-cases/htmlError.html';
    const brokenLinkUrl = 'tests/test-cases/brokenLinks.html';
    const notDocumentLinksUrl = 'tests/test-cases/notDocumentLinks.html';
    const duplicateLinkUrl = 'tests/test-cases/duplicateLinks.html';

    // eslint-disable-next-line require-await
    const testBrokenLinkTest = async (context, checkLinkResult) => {
        return executeTestWithLoggerSpy(
            context,
            brokenLinkTest,
            checkLinkResult
        );
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    afterAll(async () => {
        await closePageAndBrowser();
    });

    it('should not execute broken links test if isEnabled setting is false', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        context.urlSettings.brokenLinkTest.enabled = false;
        const { loggerSpy } = await testBrokenLinkTest(context);
        expect(loggerSpy).not.toHaveBeenCalled();
    });

    it('should execute broken links test if enabled setting is set to true', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        const { loggerSpy } = await testBrokenLinkTest(context);
        // Allow since this is just a configuration test to ensure execution, detailed
        // tests follow checking arguments.
        // eslint-disable-next-line jest/prefer-called-with
        expect(loggerSpy).toHaveBeenCalled();
    });

    it('should log a passed test result for page that has no broken links', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        const { loggerSpy } = await testBrokenLinkTest(
            context,
            linkUtils.httpResponse.ok
        );
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.passed })
        );
    });

    it('should log a failed test result for page that has broken links', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        context.urlSettings.brokenLinkTest.failWarn = false;
        const { loggerSpy } = await testBrokenLinkTest(
            context,
            linkUtils.httpResponse.unknownError
        );
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: testResultStates.failed })
        );
    });

    it('should log data with broken link errors for a page that has broken links', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        const { loggerSpy } = await testBrokenLinkTest(
            context,
            linkUtils.httpResponse.unknownError
        );
        expect(loggerSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should not log data with broken link errors for a page that does not have broken links', async () => {
        expect.assertions(1);
        const context = await getContext(brokenLinkUrl, [], true);
        const { loggerSpy } = await testBrokenLinkTest(context);
        expect(loggerSpy).not.toHaveBeenCalledWith(
            expect.objectContaining({ data: expect.any(Array) })
        );
    });

    it('should check for broken links for a page that has links', async () => {
        expect.assertions(1);
        const linkCount = 7;
        const context = await getContext(brokenLinkUrl, [], true);
        const { checkLinkSpy } = await testBrokenLinkTest(context);
        expect(checkLinkSpy).toHaveBeenCalledTimes(linkCount);
    });

    it('should not check for broken links for a page that does not have links', async () => {
        expect.assertions(1);
        const context = await getContext(noLinkUrl, [], true);
        const { checkLinkSpy } = await testBrokenLinkTest(context);
        expect(checkLinkSpy).not.toHaveBeenCalled();
    });

    it('should only check unique links for broken links', async () => {
        expect.assertions(1);
        // Test page includes 7 links with a duplicate same page link and external link
        const linkCount = 5;
        const context = await getContext(duplicateLinkUrl, [], true);
        const { checkLinkSpy } = await testBrokenLinkTest(context);
        expect(checkLinkSpy).toHaveBeenCalledTimes(linkCount);
    });

    it('should not check for broken links for a page with only links that are not documents (http/file)', async () => {
        expect.assertions(1);
        const context = await getContext(notDocumentLinksUrl, [], true);
        const { checkLinkSpy } = await testBrokenLinkTest(context);
        expect(checkLinkSpy).not.toHaveBeenCalled();
    });
});
