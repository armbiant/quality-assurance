'use strict';

const pagean = require('../index');
const configParser = require('../lib/config');
const reporter = require('../lib/reporter');
const { testResultStates } = require('../lib/test-utils');
const fileUtils = require('../lib/external-file-utils');

// eslint-disable-next-line max-lines-per-function
describe('integration tests', () => {
    const allPassConfigFileName =
        './tests/test-configs/integration-tests/all-passing-tests.pageanrc.json';
    const horizontalScrollbarTestFailConfigFileName =
        './tests/test-configs/integration-tests/horizontal-scrollbar.pageanrc.json';
    const consoleOutputTestFailConfigFileName =
        './tests/test-configs/integration-tests/console-output.pageanrc.json';
    const consoleErrorTestFailConfigFileName =
        './tests/test-configs/integration-tests/console-error.pageanrc.json';
    const consoleErrorTestFailReporterHtmlConfigFileName =
        './tests/test-configs/integration-tests/console-error-reporter-html.pageanrc.json';
    const consoleErrorTestFailReporterCliJsonConfigFileName =
        './tests/test-configs/integration-tests/console-error-reporter-cli-json.pageanrc.json';
    const renderedHtmlTestFailConfigFileName =
        './tests/test-configs/integration-tests/html-error.pageanrc.json';
    const pageLoadTimeTestFailConfigFileName =
        './tests/test-configs/integration-tests/page-load-time.pageanrc.json';
    const externalScriptTestFailConfigFileName =
        './tests/test-configs/integration-tests/external-scripts-error.pageanrc.json';
    const brokenLinksTestFailConfigFileName =
        './tests/test-configs/integration-tests/broken-links-error.pageanrc.json';
    const brokenLinksTestBrowserFailConfigFileName =
        './tests/test-configs/integration-tests/broken-links-error-browser.pageanrc.json';

    const horizontalScrollbarTestName =
        'should not have a horizontal scrollbar';
    const consoleOutputTestName = 'should not have console output';
    const consoleErrorTestName = 'should not have console errors';
    const renderedHtmlTestName = 'should have valid rendered HTML';
    const pageLoadTimeTestName = 'should load page within 2 sec';
    const externalScriptTestName = 'should not have external scripts';
    const brokenLinksTestName = 'should not have broken links';

    const pageanErrorExitCode = 2;

    const executeTests = async (configFileName) => {
        const config = configParser(configFileName);

        const consoleLogSpy = jest
            .spyOn(console, 'log')
            .mockImplementation(() => {});
        const processSpy = jest
            .spyOn(process, 'exit')
            .mockImplementation(() => {});
        const reporterSpy = jest
            .spyOn(reporter, 'saveReports')
            .mockImplementation(() => {});
        const saveScriptSpy = jest
            .spyOn(fileUtils, 'saveExternalScript')
            .mockImplementation(() => {});

        await pagean.executeAllTests(config);
        return {
            consoleLogSpy,
            processSpy,
            reporterSpy,
            saveScriptSpy
        };
    };

    const executeFailedIntegrationTest = async (
        config,
        testName,
        dataCount
    ) => {
        /* eslint-disable jest/max-expects -- allow for integration test */
        expect.assertions(dataCount ? 7 : 6); // eslint-disable-line no-magic-numbers
        const { processSpy, reporterSpy } = await executeTests(config);

        // Expect test suite to fail and exit with pagean error code
        expect(processSpy).toHaveBeenCalledWith(pageanErrorExitCode);
        expect(reporterSpy).toHaveBeenCalledTimes(1);

        const testResults = reporterSpy.mock.calls[0][0]; // eslint-disable-line prefer-destructuring -- less intuitive
        // Expect only one URL to be tested
        expect(testResults.results).toHaveLength(1);
        const failedTests = testResults.results[0].tests;
        // Expect only one test to be run
        expect(failedTests).toHaveLength(1);
        const failedTest = failedTests[0]; // eslint-disable-line prefer-destructuring -- less intuitive
        // Verify the correct test was run and failed
        expect(failedTest.name).toBe(testName);
        expect(failedTest.result).toBe(testResultStates.failed);
        if (dataCount) {
            expect(failedTest.data).toHaveLength(dataCount);
        }
        /* eslint-enable jest/max-expects */
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should execute all tests and all should pass', async () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        const { processSpy, reporterSpy, saveScriptSpy } = await executeTests(
            allPassConfigFileName
        );
        // This passes when npm test is called, but fails in other cases like watch
        // expect(processSpy).not.toHaveBeenCalled();
        expect(processSpy).not.toHaveBeenCalled();
        expect(reporterSpy).toHaveBeenCalledTimes(1);
        expect(saveScriptSpy).not.toHaveBeenCalled();
    });

    it('should execute tests and horizontal scrollbar test should fail', async () => {
        expect.hasAssertions();
        await executeFailedIntegrationTest(
            horizontalScrollbarTestFailConfigFileName,
            horizontalScrollbarTestName
        );
    });

    it('should execute tests and console output test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 1;
        await executeFailedIntegrationTest(
            consoleOutputTestFailConfigFileName,
            consoleOutputTestName,
            dataCount
        );
    });

    it('should execute tests and console error test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 1;
        await executeFailedIntegrationTest(
            consoleErrorTestFailConfigFileName,
            consoleErrorTestName,
            dataCount
        );
    });

    it('should execute tests and rendered HTML test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 2;
        await executeFailedIntegrationTest(
            renderedHtmlTestFailConfigFileName,
            renderedHtmlTestName,
            dataCount
        );
    });

    it('should execute tests and page load time test should fail', async () => {
        expect.hasAssertions();
        await executeFailedIntegrationTest(
            pageLoadTimeTestFailConfigFileName,
            pageLoadTimeTestName
        );
    });

    it('should execute tests and external script test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 4;
        await executeFailedIntegrationTest(
            externalScriptTestFailConfigFileName,
            externalScriptTestName,
            dataCount
        );
    });

    it('should execute tests and broken links test should fail', async () => {
        expect.hasAssertions();
        const dataCount = 3;
        await executeFailedIntegrationTest(
            brokenLinksTestFailConfigFileName,
            brokenLinksTestName,
            dataCount
        );
    });

    it('should execute tests and broken links test (with browser) should fail', async () => {
        expect.hasAssertions();
        const dataCount = 2;
        await executeFailedIntegrationTest(
            brokenLinksTestBrowserFailConfigFileName,
            brokenLinksTestName,
            dataCount
        );
    });

    it('should execute all tests and report failure with the correct tests failed', async () => {
        expect.assertions(5); // eslint-disable-line no-magic-numbers
        const configFileName = './.pageanrc.json';
        const totalTests = 58;
        const externalScriptCount = 4;

        const { processSpy, reporterSpy, saveScriptSpy } = await executeTests(
            configFileName
        );

        expect(processSpy).toHaveBeenCalledWith(pageanErrorExitCode);
        expect(saveScriptSpy).toHaveBeenCalledTimes(externalScriptCount);

        // eslint-disable-next-line prefer-destructuring -- less intuitive
        const testResults = reporterSpy.mock.calls[0][0];
        // Verify number of expected tests were run
        expect(testResults.summary.tests).toBe(totalTests);
        // Verify appropriate tests failed.  Some tests have log data with times and paths that
        // will fail if just checking snapshot of results, so filter down to URL and failed tests.
        const failedTests = [];
        for (const testResult of testResults.results) {
            const failed = testResult.tests
                .filter((test) => test.result === testResultStates.failed)
                .map((test) => ({ url: testResult.url, name: test.name }));
            failedTests.push(...failed);
        }
        expect(failedTests).toMatchSnapshot();

        // Check that console message field names are correct and returning data from puppeteer.
        // These contain environment-specific data, so exact test isn't checked, but the fields
        // will be empty if they are no longer reported by Chromium (see #85, #86).
        // eslint-disable-next-line prefer-destructuring
        const consoleLogTestFailResult = testResults.results[0].tests[1];
        expect(consoleLogTestFailResult.data[0]).toStrictEqual(
            expect.objectContaining({
                type: expect.any(String),
                text: expect.any(String),
                location: expect.any(Object)
            })
        );
    });

    it('should pass reporter config when saving reports', async () => {
        expect.assertions(1);
        const { reporterSpy } = await executeTests(
            consoleErrorTestFailReporterCliJsonConfigFileName
        );
        expect(reporterSpy.mock.calls[0][1]).toStrictEqual(['cli', 'json']);
    });

    it('should write all results to console if CLI reporter enabled', async () => {
        expect.assertions(1);
        const { consoleLogSpy } = await executeTests(
            consoleErrorTestFailReporterCliJsonConfigFileName
        );
        // Console should be called 3 times: url, test result, and final results
        // eslint-disable-next-line no-magic-numbers
        expect(consoleLogSpy).toHaveBeenCalledTimes(3);
    });

    it('should only summary results to console if CLI reporter disabled', async () => {
        expect.assertions(1);
        const { consoleLogSpy } = await executeTests(
            consoleErrorTestFailReporterHtmlConfigFileName
        );
        // Console should only be called once for final results
        expect(consoleLogSpy).toHaveBeenCalledTimes(1);
    });
});
