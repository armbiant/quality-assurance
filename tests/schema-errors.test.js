'use strict';

const { red } = require('kleur');
const { formatErrors } = require('../lib/schema-errors');

// eslint-disable-next-line max-lines-per-function
describe('schema errors', () => {
    const baseKey = '<pageanrc>';
    const errors = [
        {
            keyword: 'minProperties',
            instancePath: '/puppeteerLaunchOptions',
            schemaPath: '#/properties/puppeteerLaunchOptions/minProperties',
            params: { limit: 1 },
            message: 'should NOT have fewer than 1 items'
        },
        {
            keyword: 'enum',
            instancePath: '/reporters/0',
            schemaPath: '#/properties/reporters/items/enum',
            params: { allowedValues: ['cli', 'html', 'json'] },
            message: 'should be equal to one of the allowed values'
        }
    ];

    const getValuesFromErrorMessage = (error) => {
        const results = error
            .trim()
            .split('  ')
            .filter((value) => value !== '');
        return { dataKey: results[0], message: results[1] };
    };

    it('should return array of error strings', () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        const results = formatErrors(errors);
        expect(Array.isArray(results)).toBe(true);
        for (const result of results) {
            expect(typeof result).toBe('string');
        }
    });

    it('should format error strings with only data key and error message', () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        const results = formatErrors(errors);
        const { dataKey, message, ...values } = getValuesFromErrorMessage(
            results[0]
        );
        // Check that dataKey and message match the appropriate text, and there are no
        // other values in error message. Detailed formatting is checked in other tests.
        expect(dataKey).toMatch(errors[0].instancePath.replace('/', ''));
        expect(message).toMatch(errors[0].message);
        expect(values).toStrictEqual({});
    });

    it('should format error strings with red text for error message', () => {
        expect.assertions(1);
        const results = formatErrors(errors);
        const { message } = getValuesFromErrorMessage(results[0]);
        expect(message).toBe(red(errors[0].message));
    });

    it('should convert instancePath from JSON pointer to data key', () => {
        expect.assertions(1);
        const results = formatErrors(errors);
        const { dataKey } = getValuesFromErrorMessage(results[1]);
        expect(dataKey).toBe(`${baseKey}.reporters[0]`);
    });

    it('should convert empty instancePath to base data key', () => {
        expect.assertions(1);
        const data = [
            { instancePath: '', message: 'Error message', params: {} }
        ];
        const results = formatErrors(data);
        const { dataKey } = getValuesFromErrorMessage(results[0]);
        expect(dataKey).toBe(baseKey);
    });

    it('should convert root instancePath to base data key', () => {
        expect.assertions(1);
        const data = [
            { instancePath: '/', message: 'Error message', params: {} }
        ];
        const results = formatErrors(data);
        const { dataKey } = getValuesFromErrorMessage(results[0]);
        expect(dataKey).toBe(baseKey);
    });

    it('should format error messages for invalid enums with valid values', () => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        const results = formatErrors(errors);
        const { message } = getValuesFromErrorMessage(results[1]);
        const [, error] = errors;
        expect(message).toMatch(error.message);
        const allowedValue = error.params.allowedValues.join(', ');
        expect(message).toMatch(allowedValue);
    });
});
