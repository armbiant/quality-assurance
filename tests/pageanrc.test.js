'use strict';

const bin = require('bin-tester');
const stripAnsi = require('strip-ansi');

// These names do not use path.join since these names resolve in all OSs, and are
// included in snapshots. With path.join there would be a Linux/Windows mismatch
// in tests (the snapshots matching the OS they were created in).
const cliTestFolder = './tests/test-configs/cli-tests';
const defaultConfigFileName = '.pageanrc.json';
const nonExistentConfigFileName = 'not-a-file.json';
const invalidConfigFileName =
    './tests/test-configs/unit-tests/invalid.pageanrc.json';
const emptyConfigFileName =
    './tests/test-configs/unit-tests/empty.pageanrc.json';
const allEmptyConfigFileName =
    './tests/test-configs/cli-tests/all-empty.pageanrc.json';
const allErrorsConfigFileName =
    './tests/test-configs/cli-tests/all-errors.pageanrc.json';

const errorMessage = 'Error linting pageanrc file';
const lintCommand = 'pageanrc-lint';

// eslint-disable-next-line max-lines-per-function
describe('pageanrc lint', () => {
    const testLintInvalidFiles = async (args, message) => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        const result = await bin(
            args,
            undefined,
            undefined,
            undefined,
            lintCommand
        );
        expect(result.code).toBe(1);
        expect(result.error.message).toMatch(message);
        expect(result.stderr).toMatch(errorMessage);
    };

    it('should return an error and exit for a non existent pageanrc file', async () => {
        expect.hasAssertions();
        const args = [nonExistentConfigFileName];
        await testLintInvalidFiles(args, 'ENOENT');
    });

    it('should return an error and exit for an invalid pageanrc file', async () => {
        expect.hasAssertions();
        const args = [invalidConfigFileName];
        await testLintInvalidFiles(args, 'Unexpected end of JSON input');
    });

    const testLintWithErrorSnapshot = async (args) => {
        expect.assertions(1);
        const { stderr } = await bin(
            args,
            undefined,
            undefined,
            undefined,
            lintCommand
        );
        // Strip ansi escape codes since not serialized in jest snapshots
        expect(stripAnsi(stderr)).toMatchSnapshot('stderr');
    };

    const testLintWithOutputSnapshot = async (args) => {
        expect.assertions(1);
        const { stdout } = await bin(
            args,
            undefined,
            undefined,
            undefined,
            lintCommand
        );
        expect(stdout).toMatchSnapshot('stdout');
    };

    it('should lint the pageanrc file specified as argument', async () => {
        expect.hasAssertions();
        const { stderr } = await bin(
            [allErrorsConfigFileName],
            undefined,
            undefined,
            undefined,
            lintCommand
        );
        expect(stderr).toMatch(allErrorsConfigFileName);
    });

    it('should lint the default file if none specified', async () => {
        expect.assertions(1);
        const { stdout } = await bin(
            [],
            cliTestFolder,
            undefined,
            undefined,
            lintCommand
        );
        expect(stdout).toMatch(defaultConfigFileName);
    });

    it('should lint the all errors pageanrc and return all CLI errors', async () => {
        expect.hasAssertions();
        const args = [allErrorsConfigFileName];
        await testLintWithErrorSnapshot(args);
    });

    it('should lint the all empty pageanrc and return all CLI errors', async () => {
        expect.hasAssertions();
        const args = [allEmptyConfigFileName];
        await testLintWithErrorSnapshot(args);
    });

    it('should lint the empty pageanrc and return all CLI errors', async () => {
        expect.hasAssertions();
        const args = [emptyConfigFileName];
        await testLintWithErrorSnapshot(args);
    });

    it('should lint the all errors pageanrc and return all JSON errors to stdout with -j option', async () => {
        expect.hasAssertions();
        const args = ['-j', allErrorsConfigFileName];
        await testLintWithOutputSnapshot(args);
    });

    it('should lint the all empty pageanrc and return all JSON errors to stdout with --json option', async () => {
        expect.hasAssertions();
        const args = ['--json', allEmptyConfigFileName];
        await testLintWithOutputSnapshot(args);
    });
});
