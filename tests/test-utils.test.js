/* eslint-disable max-lines */
'use strict';

const { testResultStates, pageanTest } = require('../lib/test-utils');

describe('testResultStates', () => {
    it('should include a passed state', () => {
        expect.assertions(1);
        expect(testResultStates.passed).toBeDefined();
    });

    it('should include a warning state', () => {
        expect.assertions(1);
        expect(testResultStates.warning).toBeDefined();
    });

    it('should include a failed state', () => {
        expect.assertions(1);
        expect(testResultStates.failed).toBeDefined();
    });

    it('should provide string values for all states', () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        for (const state of Object.keys(testResultStates)) {
            expect(typeof testResultStates[state]).toBe('string');
        }
    });
});

// eslint-disable-next-line max-lines-per-function
describe('pageanTest', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should provide a template test function', () => {
        expect.assertions(1);
        expect(typeof pageanTest).toBe('function');
    });

    // eslint-disable-next-line max-lines-per-function
    const executeTestWithMocks = async (testArguments) => {
        const {
            testIsEnabledSettingValue = true,
            testIsFailWarnValue = false,
            ...otherArguments
        } = testArguments;
        const defaultTestArguments = {
            testName: 'should run a test',
            testSettingProperty: 'testIsEnabledSetting',
            // Include context argument as a reminder of the argument passed even though
            // this mock test function does not use it.
            // eslint-disable-next-line no-unused-vars
            testFunction: (context) => ({ result: testResultStates.passed }),
            testContext: {
                urlSettings: {
                    testIsEnabledSetting: {
                        enabled: testIsEnabledSettingValue,
                        failWarn: testIsFailWarnValue
                    }
                },
                logger: {
                    logTestResults: () => {}
                }
            }
        };
        const finalTestArguments = {
            ...defaultTestArguments,
            ...otherArguments
        };
        const mockTestFunction = jest
            .fn()
            .mockImplementationOnce(finalTestArguments.testFunction);
        let logTestResultsSpy;
        if (finalTestArguments.testContext.logger) {
            logTestResultsSpy = jest.spyOn(
                finalTestArguments.testContext.logger,
                'logTestResults'
            );
        }

        await pageanTest(
            finalTestArguments.testName,
            mockTestFunction,
            finalTestArguments.testContext,
            finalTestArguments.testSettingProperty
        );

        return {
            mockTestFunction,
            logTestResultsSpy
        };
    };

    it('should throw if testSettingProperty is not specified', async () => {
        expect.assertions(1);
        await expect(
            executeTestWithMocks({ testSettingProperty: undefined })
        ).rejects.toThrow('undefined');
    });

    it('should throw for invalid testSettingProperty', async () => {
        expect.assertions(1);
        const settingName = 'invalidSetting';
        await expect(
            executeTestWithMocks({ testSettingProperty: settingName })
        ).rejects.toThrow(settingName);
    });

    it('should execute test function for valid testSettingProperty that is true', async () => {
        expect.assertions(1);
        const { mockTestFunction } = await executeTestWithMocks({
            testSettingProperty: 'testIsEnabledSetting',
            testIsEnabledSettingValue: true
        });
        // eslint-disable-next-line jest/prefer-called-with
        expect(mockTestFunction).toHaveBeenCalled();
    });

    it('should not execute test function for valid testSettingProperty that is false', async () => {
        expect.assertions(1);
        const { mockTestFunction } = await executeTestWithMocks({
            testSettingProperty: 'testIsEnabledSetting',
            testIsEnabledSettingValue: false
        });
        expect(mockTestFunction).not.toHaveBeenCalled();
    });

    it('should throw if testContext is undefined', async () => {
        expect.assertions(1);
        await expect(
            executeTestWithMocks({ testContext: undefined })
        ).rejects.toThrow('logger');
    });

    it('should throw if provided testContext does not have a urlSettings property', async () => {
        expect.assertions(1);
        const settingName = 'testIsEnabledSetting';
        await expect(
            executeTestWithMocks({
                testSettingProperty: settingName,
                testContext: {
                    logger: {
                        logTestResults: () => {}
                    }
                }
            })
        ).rejects.toThrow(settingName);
    });

    it('should throw if provided testContext does not have a logger property', async () => {
        expect.assertions(1);
        const settingName = 'testIsEnabledSetting';
        await expect(
            executeTestWithMocks({
                testSettingProperty: settingName,
                testContext: {
                    urlSettings: {
                        testIsEnabledSetting: { enabled: true }
                    }
                }
            })
        ).rejects.toThrow('logTestResults');
    });

    it('should throw if provided testContext.logger is an invalid object', async () => {
        expect.assertions(1);
        const settingName = 'testIsEnabledSetting';
        await expect(
            executeTestWithMocks({
                testSettingProperty: settingName,
                testContext: {
                    urlSettings: {
                        testIsEnabledSetting: { enabled: true }
                    },
                    logger: {
                        logTestResults: true
                    }
                }
            })
        ).rejects.toThrow('logTestResults');
    });

    it('should execute testFunction passing testContext', async () => {
        expect.assertions(1);
        const testContext = {
            urlSettings: {
                testIsEnabledSetting: { enabled: true }
            },
            logger: {
                logTestResults: () => {}
            }
        };
        const { mockTestFunction } = await executeTestWithMocks({
            testContext
        });
        expect(mockTestFunction).toHaveBeenCalledWith(testContext);
    });

    it('should add a testSettings property to the provided testContext', async () => {
        expect.assertions(1);
        const { mockTestFunction } = await executeTestWithMocks({
            testSettingProperty: 'testIsEnabledSetting',
            testIsEnabledSettingValue: true
        });
        expect(mockTestFunction).toHaveBeenCalledWith(
            expect.objectContaining({ testSettings: expect.any(Object) })
        );
    });

    it('should log passed result for passing test', async () => {
        expect.assertions(1);
        const result = testResultStates.passed;
        const testFunction = () => ({ result });
        const { logTestResultsSpy } = await executeTestWithMocks({
            testFunction
        });
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result })
        );
    });

    it('should log failed result for failing test', async () => {
        expect.assertions(1);
        const result = testResultStates.failed;
        const testFunction = () => ({ result });
        const { logTestResultsSpy } = await executeTestWithMocks({
            testFunction
        });
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result })
        );
    });

    it('should log warning result for failing test with failWarn true', async () => {
        expect.assertions(1);
        const testResult = testResultStates.failed;
        const loggedResult = testResultStates.warning;
        const testFunction = () => ({ result: testResult });
        const { logTestResultsSpy } = await executeTestWithMocks({
            testFunction,
            testIsFailWarnValue: true
        });
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result: loggedResult })
        );
    });

    it('should log passed result for passing test with failWarn true', async () => {
        expect.assertions(1);
        const result = testResultStates.passed;
        const testFunction = () => ({ result });
        const { logTestResultsSpy } = await executeTestWithMocks({
            testFunction,
            testIsFailWarnValue: true
        });
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result })
        );
    });

    const executeTestThatTrows = async (
        errorMessage = 'test failed',
        testIsFailWarnValue = false
    ) => {
        const testFunction = () => {
            throw new Error(errorMessage);
        };
        const { logTestResultsSpy } = await executeTestWithMocks({
            testFunction,
            testIsFailWarnValue
        });
        return logTestResultsSpy;
    };

    it('should log failed results for failing test if testFunction throws', async () => {
        expect.assertions(1);
        const result = testResultStates.failed;
        const logTestResultsSpy = await executeTestThatTrows();
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result })
        );
    });

    it('should log warning results for failing test if testFunction throws and failWarn is true', async () => {
        expect.assertions(1);
        const result = testResultStates.warning;
        const testIsFailWarn = true;
        const logTestResultsSpy = await executeTestThatTrows(
            '',
            testIsFailWarn
        );
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result })
        );
    });

    it('should log error as data if testFunction throws', async () => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        const errorMessage = 'this test failed';
        const logTestResultsSpy = await executeTestThatTrows(errorMessage);
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ data: { error: expect.any(Error) } })
        );
        expect(logTestResultsSpy.mock.calls[0][0].data.error.message).toBe(
            errorMessage
        );
    });

    it('should log results with given test name', async () => {
        expect.assertions(1);
        const { logTestResultsSpy } = await executeTestWithMocks({});
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ name: 'should run a test' })
        );
    });

    it('should log results with updated test name if overridden in testFunction', async () => {
        expect.assertions(1);
        const testName = 'should have a proper test name';
        const { logTestResultsSpy } = await executeTestWithMocks({ testName });
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ name: testName })
        );
    });

    it('should log results with test data if provided', async () => {
        expect.assertions(1);
        const result = testResultStates.passed;
        const data = [{ foo1: 'bar1' }, { foo1: 'bar2' }];
        const testFunction = () => ({ result, data });
        const { logTestResultsSpy } = await executeTestWithMocks({
            testFunction
        });
        expect(logTestResultsSpy).toHaveBeenCalledWith(
            expect.objectContaining({ result, data })
        );
    });
});
