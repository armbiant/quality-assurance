# Pin to the node active LTS release
FROM registry.gitlab.com/gitlab-ci-utils/docker-puppeteer:node-16@sha256:0c61bc4b7abc300ae8a55f5a3ae4f9fe6c21df65a8285f34bf381367a22db0b9

ENV NODE_ENV=production

# Image uses a lesser privileged account, but need root to
# install Puppeteer with npm dependencies
USER root

# Copy application files and install dependencies
# Include http-server and wait-on to be able to run globally.
RUN mkdir -p /pagean
COPY . /pagean/
WORKDIR /pagean

# Always install the latest version for standalone applications.
# hadolint ignore=DL3016
RUN npm ci --only=production && \
    npm link && \
    npm install -g http-server wait-on

# Return to original lesser privileged account
USER pptruser

CMD [ "pagean" ]
