#!/usr/bin/env node
'use strict';

const { log, Levels } = require('ci-logger');
const program = require('commander');
const { green, underline } = require('kleur');
const { lintConfigFile } = require('../lib/config');
const { formatErrors } = require('../lib/schema-errors');
const pkg = require('../package.json');

const defaultConfigFileName = './.pageanrc.json';

program
    .version(pkg.version)
    .option('-j, --json', 'output JSON with full details')
    .description('Lint a pageanrc file')
    .usage(`[options] [file] (default: "${defaultConfigFileName}")`)
    .parse(process.argv);
const options = program.opts();

const logError = (message, exitOnError = true) => {
    log({ message, level: Levels.Error, exitOnError, errorCode: 1 });
};

const outputJsonResults = (configFileName, lintResults) => {
    // Log JSON to stdout for consistency, and eliminates extraneous stderr
    // output in PowerShell which produces invalid JSON if piped to file.
    // eslint-disable-next-line no-magic-numbers
    log({ message: JSON.stringify(lintResults.errors, undefined, 2) });
    if (!lintResults.isValid) {
        logError(`Errors found in file ${configFileName}`);
    }
};

const outputConsoleResults = (configFileName, lintResults) => {
    if (lintResults.isValid) {
        log({ message: `\n${configFileName}: ${green('valid')}\n` });
    } else {
        logError(`\n${underline(configFileName)}`, false);
        for (const error of formatErrors(lintResults.errors)) {
            logError(error, false);
        }
        // Add line for spacing and error exits process
        logError('');
    }
};

try {
    const configFileName =
        program.args.length > 0 ? program.args[0] : defaultConfigFileName;
    const lintResults = lintConfigFile(configFileName);

    if (options.json) {
        outputJsonResults(configFileName, lintResults);
    } else {
        outputConsoleResults(configFileName, lintResults);
    }
} catch (error) {
    logError(`Error linting pageanrc file\n${error.message}`);
}
