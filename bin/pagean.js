#!/usr/bin/env node
'use strict';

const { log, Levels } = require('ci-logger');
const program = require('commander');
const pagean = require('../index');
const getConfig = require('../lib/config');
const pkg = require('../package.json');

const defaultConfigFileName = './.pageanrc.json';

program
    .version(pkg.version)
    .option(
        '-c, --config <file>',
        'the path to the pagean configuration file',
        defaultConfigFileName
    )
    .parse(process.argv);
const options = program.opts();

try {
    const config = getConfig(options.config);
    pagean.executeAllTests(config);
} catch (error) {
    log({
        message: `Error executing pagean tests\n${error.message}`,
        level: Levels.Error,
        exitOnError: true,
        errorCode: 1
    });
}
